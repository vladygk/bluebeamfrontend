import emptyCardboard from "/src/assets/photos/emptyCardboard.png";
import beanBox from "/src/assets/photos/error-page-1.png";
import { FC } from "react";
import { Link } from "react-router-dom";

export const Error: FC = () => {
  return (
    <div className="text-6xl text-center sm:text-4xl">
      <h1 className="font-bold p-12">Error 404</h1>
      <h2 className="p-12">Oops, this page doesn't exist.</h2>
      <div className="flex justify-center">
        <img
          className="w-[35%] md:w-[60%] sm:w-[80%]"
          src={beanBox}
          alt="emptyCardboard imagess"
        />
      </div>
      <Link to="/" className="btn btn-secondary text-lg">
        Go back
      </Link>
    </div>
  );
};
