import { FC, useContext } from "react";
import { ThemeContext } from "../../contexts";

import "./LoadingScreen.css";

import logo from "../../assets/logos/logo.svg";
import darkLogo from "../../assets/logos/darkLogo.svg";

export const LoadingScreen: FC = () => {
  const { theme } = useContext(ThemeContext);

  return (
    <>
      <div className="flex flex-col mt-20 sm:mt-10  min-h-[70vh] z-10 items-center">
        <div className="spinner-box w-[150px] relative bg-transparent">
          <div className="circle-border w-[150px] h-[150px] p-[3px] flex justify-center items-center rounded-[50%] bg-primary ">
            <div className="circle-core w-full h-full bg-primary rounded-[50%] animate-none"></div>
          </div>
          <img
            src={theme === "lightTheme" ? logo : darkLogo}
            className="logo-image h-[100px] absolute top-[21%] left-[19%] rounded-full"
          ></img>
        </div>
        <h2 className="text-xl font-semibold mt-5">Loading Blue beans...</h2>
      </div>
    </>
  );
};
