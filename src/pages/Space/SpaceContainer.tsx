import { FC, useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { RxDividerVertical } from "react-icons/rx";
import { Button } from "semantic-ui-react";
import { Space } from "./Space";
import { SimpleTable } from "../../components/FacilitySummarySection/SimpleTable";

import { IFetchedSpace, getOneSpace } from "../../services/SpaceServices";
import { SpaceContext } from "../../contexts/ISpaceContext";

export const SpaceContainer: FC = () => {
  const [space, setSpace] = useState<IFetchedSpace>({
    id: -1,
    name: "",
    location: "",
    imageUrl: "",
    facilityId: -1,
  });

  const { facilityId, spaceId } = useParams();
  const { activeButton, setActiveButton } = useContext(SpaceContext)!;

  useEffect(() => {
    getOneSpace(Number(spaceId), Number(facilityId)).then((f) => {
      if (f !== undefined) {
        setSpace(f);
      }
    });
  }, []);

  useEffect(() => {
    return () => {
      setActiveButton("space");
    };
  }, []);

  function renderDefault() {
    setActiveButton("space");
  }

  function renderPunches() {
    setActiveButton("punches");
  }

  function renderAssets() {
    setActiveButton("assets");
  }

  return (
    <div
      className={`flex flex-col w-full px-4 py-4 mx-auto rounded-md min-h-[calc(100svh-88px)]`}
    >
      <div className="mx-10 sm:mx-0 ">
        <h2 className="text-4xl text-accent pt-3 pb-10">{space.name}</h2>

        <div className="flex justify-between items-center pb-4 h-[10%]"></div>
        <div className="flex justify-start mb-5 sm:flex-wrap w-full">
          <Button
            className={`text-accent btn btn-ghost text-lg ${
              activeButton === "space" ? "bg-info" : ""
            } `}
            onClick={renderDefault}
          >
            Space
          </Button>
          <div className="text-[25px] self-center">
            <RxDividerVertical />
          </div>
          <Button
            className={`text-accent btn btn-ghost text-lg ${
              activeButton === "punches" ? "bg-info" : ""
            } `}
            onClick={renderPunches}
          >
            Punches
          </Button>
          <div className="text-[25px] self-center">
            <RxDividerVertical />
          </div>
          <Button
            className={`text-accent btn btn-ghost text-lg ${
              activeButton === "assets" ? "bg-info" : ""
            } `}
            onClick={renderAssets}
          >
            Assets
          </Button>
        </div>
      </div>
      <div className="mx-10 sm:mx-0">
        {activeButton == "space" && <Space />}
        {activeButton === "punches" && <SimpleTable name="punches" />}
        {activeButton === "assets" && <SimpleTable name="assets" />}
      </div>
    </div>
  );
};
