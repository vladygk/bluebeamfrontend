import { useContext, useState, ChangeEvent, FC, useEffect } from "react";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import { Input } from "../../components/Input/";
import { validate } from "../../utils/formValidation";
import { AuthContext, ThemeContext } from "../../contexts/";
import { IHasError } from "../../utils/formValidation";

import register from "../../assets/photos/laptop-tasks.svg";
import { ITokenPayload, decodeToken } from "../../utils/tokenUtils";
import { getUserIdFromStorage, saveToStorage } from "../../utils/localStorage";
import { addUserToFacility } from "../../services/auth";
import * as CryptoJS from "crypto-js";
import { encryptionKey } from "../../utils/common";
export interface IValueRegister {
  name: string;
  email: string;
  password: string;
  role: string;
}

export const Register: FC = () => {
  const [isRegistering, setIsRegistering] = useState(false);
  const [facilityIdParam, setFacilityIdParam] = useState("");

  const [searchParams, setSearchParams] = useSearchParams();
  const navigate = useNavigate();

  const [value, setValue] = useState<IValueRegister>({
    name: "",
    email: "",
    password: "",
    role: "2",
  });

  const [error, setError] = useState<IHasError>({
    email: false,
    password: false,
    name: false,
    repeatPassword: false,
  });
  const {
    onRegister,
    responseStatusError,
    setUserName,
    setUserId,
    setUserRole,
    setResponseStatusError,
  } = useContext(AuthContext)!;

  const [repeatPassword, setRepeatPassword] = useState<string>("");
  const [showPassword, setShowPassword] = useState<boolean>(false);

  useEffect(() => {
    setResponseStatusError("");
    //console.log(searchParams.get("encrypted"));

    if (searchParams.size > 0) {
      try {
        const params = CryptoJS.AES.decrypt(
          searchParams.get("encrypted")!,
          encryptionKey,
          { mode: CryptoJS.mode.ECB }
        );
        const base64 = CryptoJS.enc.Base64.stringify(params);
        console.log(base64);

        const urlArgs = atob(base64).split("&");
        const email = urlArgs[0].split("=")[1];
        const role = urlArgs[1].split("=")[1];
        const facilityId = urlArgs[2].split("=")[1].replace('"', "");

        if (!email || !role || !facilityId) {
          throw new Error();
        }

        setValue((prevState) => ({
          ...prevState,
          ["email"]: email,
          ["role"]: role,
        }));

        setFacilityIdParam(facilityId);
      } catch {
        navigate("/error");
      }
    }
  }, [navigate, searchParams, setResponseStatusError]);

  const showPasswordHandler = () => {
    setShowPassword(!showPassword);
  };

  const onRegisterHandler = async (e: any) => {
    setIsRegistering(true);
    await onRegister(e, value, error, repeatPassword);
    setIsRegistering(false);
    console.log(value);

    if (!responseStatusError) {
      const decodeTokenData: ITokenPayload = await decodeToken();
      if (setUserName) {
        const name = decodeTokenData["name"];
        setUserName(name);
        const userId = decodeTokenData["nameidentifier"];
        setUserId(userId);
        const role = decodeTokenData["role"]!;
        setUserRole(role);

        saveToStorage("userRole", role);
        saveToStorage("userName", name);
        saveToStorage("userId", userId);

        if (searchParams.size > 0) {
          await addUserToFacility(
            Number(userId),
            Number(facilityIdParam) //facilityId
          );
        }
      }
      navigate("/");
    }
  };

  const onChangeHandler = (e: ChangeEvent<HTMLInputElement>) =>
    setValue((state) => ({ ...state, [e.target.name]: e.target.value }));

  const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectOption = e.target.value;

    setValue((prevState) => ({
      ...prevState,
      [e.target.name]: selectOption,
    }));
  };

  return (
    <div className="grid grid-cols-2 gap-10 py-24 px-20 md:grid-cols-1 md:py-20 md:px-10 md:gap-14 sm:py-10 sm:px-8 ">
      <div className="flex items-center justify-center md:order-2 sm:hidden">
        <img
          src={register}
          className="rounded-sm w-full md:w-3/4 sm:w-full"
          alt="register-image"
        />
      </div>
      <div className="w-2/3 border-2 border-success px-7 mx-auto rounded-md sm:w-full">
        <h2 className="m-6 text-center text-2xl text-accent font-bold">
          Register
        </h2>
        <div className="my-5">
          <form onSubmit={onRegisterHandler}>
            <Input
              labelText="Name:"
              type="text"
              placeholder="Enter Name"
              name="name"
              onChange={onChangeHandler}
              value={value["name"]}
              hasError={error["name"] ?? false}
              onBlur={() => validate(value["name"], "name", setError)}
            ></Input>
            <Input
              labelText="Email:"
              type="text"
              placeholder="Enter Email"
              disabled={searchParams.size > 0}
              name="email"
              onChange={
                searchParams.size > 0
                  ? () => {
                      console.log();
                    }
                  : onChangeHandler
              }
              value={value["email"]}
              hasError={error["email"]}
              onBlur={() => validate(value["email"], "email", setError)}
            ></Input>
            <Input
              labelText="Password:"
              type={showPassword ? "text" : "password"}
              placeholder="Enter Password"
              name="password"
              onChange={onChangeHandler}
              value={value["password"]}
              hasError={error["password"]}
              onBlur={() => validate(value["password"], "password", setError)}
            ></Input>
            <Input
              labelText="Repeat Password:"
              type={showPassword ? "text" : "password"}
              placeholder="Repeat Password"
              name="repeatPassword"
              onChange={(e) => setRepeatPassword(e.target.value)}
              value={repeatPassword}
              hasError={error["repeatPassword"] ?? false}
              onBlur={() => {
                if (repeatPassword !== value["password"]) {
                  setError((state) => ({ ...state, repeatPassword: true }));
                } else {
                  setError((state) => ({ ...state, repeatPassword: false }));
                }
              }}
            ></Input>
            <>
              <label htmlFor="role" className="font-bold text-accent">
                Role:
              </label>
              <select
                onChange={
                  searchParams.size > 0
                    ? () => {
                        console.log();
                      }
                    : handleSelectChange
                }
                disabled={searchParams.size > 0}
                value={value["role"]}
                className="border-2 w-full rounded-md h-10 pl-2 bg-primary text-accent"
                id="role"
                name="role"
              >
                <option key="2" value="2">
                  Owner
                </option>
                <option key="3" value="3">
                  Worker
                </option>
              </select>
            </>
            <div className="flex items-center mt-4">
              <input
                className="mr-2 checkbox checkbox-secondary checkbox-sm"
                type="checkbox"
                onClick={showPasswordHandler}
              />
              <label htmlFor="showPassword" className="text-accent">
                Show Password
              </label>
            </div>
            <div>
              <button className="btn btn-secondary w-full my-5" type="submit">
                {isRegistering ? "Registering beans ..." : "Submit"}{" "}
              </button>
              {responseStatusError !== "" && (
                <span className=" text-error">{responseStatusError}</span>
              )}
              <p className="text-center text-accent">
                Already have an account?
                <Link className="text-blue-500 m1" to="/login">
                  Log in
                </Link>
              </p>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
