import { useEffect } from "react";
import PunchesTable from "./PunchesTable";
import { getOneFacility } from "../../services/FacilityServices";
import { Link, useNavigate, useParams } from "react-router-dom";

export const Punches = () => {
  const { facilityId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    getOneFacility(Number(facilityId)).catch((e) => navigate("/error"));
  }, []);

  return (
    <div className="w-[90%] mx-16 my-8 min-h-[calc(100svh-88px)] max-w-[calc(100svw-10.5rem)] md:w-[95%] md:mx-12 sm:mx-0 sm:px-5 sm:mb-5 sm:w-full sm:max-w-full ">
      <div className="flex justify-between sm:flex-col sm:text-center sm:items-center">
        <h2 className="text-3xl text-accent text-bold mb-16 sm:mb-10">
          Punches
        </h2>
        <Link
          to={`/facility/${Number(facilityId)}/spaces`}
          className="btn btn-secondary text-lg  sm:mb-10"
        >
          Add Punch
        </Link>
      </div>
      <PunchesTable></PunchesTable>
    </div>
  );
};
