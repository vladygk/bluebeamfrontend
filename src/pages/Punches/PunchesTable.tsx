import { useEffect, useContext, useMemo, useState, FC, Suspense } from "react";

//MRT Imports
import { MantineReactTable, MRT_ColumnDef } from "mantine-react-table";

//Mantine Imports
import { Box } from "@mantine/core";

//Date Picker Imports
import { DatePickerInput } from "@mantine/dates";

// Data
import { GetAllForFacility, GetOne, remove } from "../../services/punch";
import { IPoint } from "../../components/Drawing/IPoint";
import { useNavigate, useParams } from "react-router-dom";
import { ThemeContext } from "../../contexts";
import { LoadingScreen } from "../LoadingScreen/LoadingScreen";

export const PunchesTable: FC = () => {
  const [punches, setPunches] = useState<IPoint[]>([]);
  const [isSmallScreen, setIsSmallScreen] = useState(false);
  const { facilityId } = useParams();
  const { toggleMantineTable, theme } = useContext(ThemeContext);

  useEffect(() => {
    GetAllForFacility(Number(facilityId)).then((f) => {
      if (f !== undefined) {
        setPunches(f);
      }
    });
  }, []);

  useEffect(() => {
    toggleMantineTable();
  }, [theme, toggleMantineTable, punches]);

  useEffect(() => {
    const mediaQuery = window.matchMedia("(max-width: 1024px)");
    setIsSmallScreen(mediaQuery.matches);

    const handleMediaQueryChange = (e: MediaQueryListEvent) =>
      setIsSmallScreen(e.matches);
    mediaQuery.addEventListener("change", handleMediaQueryChange);

    // Remove event listener on unmount
    return () => {
      mediaQuery.removeEventListener("change", handleMediaQueryChange);
    };
  }, []);

  const handlePunchDelete = async (id: number) => {
    await remove(id);
    setPunches((prevPunches) => prevPunches.filter((punch) => punch.id !== id));
  };

  const navigate = useNavigate();
  const handlePunchDetails = async (id: number) => {
    const clickedPunch = await GetOne(id);
    console.log(clickedPunch);
    navigate(`/facility/${facilityId}/spaces/${clickedPunch?.spaceId}`, {
      state: clickedPunch,
    });
  };

  const columns = useMemo<MRT_ColumnDef<IPoint>[]>(
    () => [
      {
        id: "punch", //id used to define `group` column
        header: "",
        columns: [
          {
            accessorFn: (row) => `${row.title}`, //accessorFn used to join multiple data into a single cell
            id: "title", //id is still required when using accessorFn instead of accessorKey
            header: "Title",
            size: isSmallScreen ? 40 : 200,
            Cell: ({ renderedCellValue, row }) => (
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: "16px",
                }}
              >
                <span>{renderedCellValue}</span>
              </Box>
            ),
          },
          {
            accessorFn: (row) => `${row.spaceId}`, //accessorFn used to join multiple data into a single cell
            id: "space", //id is still required when using accessorFn instead of accessorKey
            header: "Space ID",
            size: isSmallScreen ? 40 : 200,
            Cell: ({ renderedCellValue, row }) => (
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: "16px",
                }}
              >
                <span>{renderedCellValue}</span>
              </Box>
            ),
          },
        ],
      },
      {
        id: "id",
        header: "",
        columns: [
          {
            accessorFn: (row) => `${row.status}`,
            accessorKey: "status",
            header: "Status",
            size: isSmallScreen ? 40 : 200,
            //custom conditional format and styling
            Cell: ({ cell }) => (
              <Box
                sx={(theme) => ({
                  backgroundColor:
                    cell.getValue<string>() == "Unassigned"
                      ? theme.colors.red[8]
                      : cell.getValue<string>() == "In progress"
                      ? theme.colors.yellow[8]
                      : theme.colors.green[8],
                  borderRadius: "4px",
                  color: "#fff",
                  maxWidth: "11ch",
                  padding: "4px",
                  textAlign: "center",
                })}
              >
                <span>{cell.getValue<string>()}</span>
                {/* {cell.getValue<string>()?.toLocaleString?.("en-US")} */}
              </Box>
            ),
          },
          {
            accessorFn: (row) =>
              `${row.assignee == null ? "None" : row.assignee}`,
            accessorKey: "assignee",
            header: "Assignee",
            size: isSmallScreen ? 40 : 200,
          },
          {
            accessorFn: (row) => new Date(row.startDate), //convert to Date for sorting and filtering
            id: "createdOn",
            header: "Created On",
            filterFn: "lessThanOrEqualTo",
            sortingFn: "datetime",
            enableColumnFilter: false, // could disable just this column's filter
            size: isSmallScreen ? 40 : 200,
            Cell: ({ cell }) => cell.getValue<Date>()?.toLocaleDateString(), //render Date as a string
            Header: ({ column }) => <em>{column.columnDef.header}</em>, //custom header markup
            //Custom Date Picker Filter from @mantine/dates
            Filter: ({ column }) => (
              <DatePickerInput
                placeholder="Filter by Created On"
                onChange={(newValue: Date) => {
                  column.setFilterValue(newValue);
                }}
                value={column.getFilterValue() as Date}
                modalProps={{ withinPortal: true }}
              />
            ),
          },
          {
            accessorFn: (row) => new Date(row.expirationDate), //convert to Date for sorting and filtering
            id: "endDate",
            header: "End Date",
            filterFn: "lessThanOrEqualTo",
            sortingFn: "datetime",
            enableColumnFilter: false, // could disable just this column's filter
            size: isSmallScreen ? 40 : 200,
            Cell: ({ cell }) => cell.getValue<Date>()?.toLocaleDateString(), //render Date as a string
            Header: ({ column }) => <em>{column.columnDef.header}</em>, //custom header markup
            //Custom Date Picker Filter from @mantine/dates
            Filter: ({ column }) => (
              <DatePickerInput
                placeholder="Filter by End Date"
                onChange={(newValue: Date) => {
                  column.setFilterValue(newValue);
                }}
                value={column.getFilterValue() as Date}
                modalProps={{ withinPortal: true }}
              />
            ),
          },
          {
            accessorFn: (row) => `${row.id}`,
            header: "Action",
            size: isSmallScreen ? 40 : 200,
            enableSorting: false, // disable sorting for this column
            enableColumnFilter: false, // could disable just this column's filter
            Cell: ({ cell }) => (
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: "16px",
                }}
              >
                <button
                  className="btn btn-neutral py-0"
                  onClick={() =>
                    handlePunchDetails(Number(cell.getValue<number>()))
                  }
                >
                  Details
                </button>

                <button
                  className="btn btn-neutral py-0"
                  onClick={() =>
                    handlePunchDelete(Number(cell.getValue<number>()))
                  }
                >
                  Delete
                </button>
              </Box>
            ),
          },
        ],
      },
    ],
    [isSmallScreen]
  );

  if (punches.length === 0) {
    return <LoadingScreen />;
  }

  return (
    <Suspense fallback={<LoadingScreen />}>
      <MantineReactTable
        columns={columns}
        data={punches}
        enableColumnFilterModes
        // enableRowSelection
        enableHiding={false}
        enableFullScreenToggle={false}
        enableColumnActions={false}
        initialState={{
          showColumnFilters: false,
          pagination: { pageSize: 5, pageIndex: 0 },
        }}
        positionToolbarAlertBanner="bottom"
        renderDetailPanel={({ row }) => (
          <Box
            sx={{
              display: "flex",
              justifyContent: "start",
              alignItems: "center",
            }}
          >
            <Box>
              <p className="text-lg text-bold">Punch description:</p>
              <p> {row.original.description}</p>
            </Box>
          </Box>
        )}
      />
    </Suspense>
  );
};

export default PunchesTable;
