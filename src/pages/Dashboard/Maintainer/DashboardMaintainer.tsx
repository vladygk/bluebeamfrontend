import { FC } from "react";
import { AllUnsassignedPunches } from "./AllUnassignedPunches";
import { MaintainerAssignedPunches } from "./MaintainerAssignedPunches";

export const DashboardMaintainer: FC = () => {
  return (
    <>
      <MaintainerAssignedPunches />
      <AllUnsassignedPunches />
    </>
  );
};
