import React, { FC } from "react";
import { SimpleTable } from "../../../components/FacilitySummarySection/SimpleTable";

export const MaintainerAssignedPunches: FC = () => {
  return (
    <>
      <SimpleTable name="punches" limit={5} />
    </>
  );
};
