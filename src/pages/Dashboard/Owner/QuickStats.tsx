import { FC, useEffect, useState } from "react";
import { IPoint } from "../../../components/Drawing/IPoint";
import { RadialProgress } from "./RadialProgress";

interface IQuickStats {
  punches: IPoint[];
}

export const QuickStats: FC<IQuickStats> = ({ punches }) => {
  const [completedPunchesCountPercent, setCompletedPunchesCountPercent] =
    useState<number | null>(null);
  const [inProgressPunchesCountPercent, setInProgressPunchesCountPercent] =
    useState<number | null>(null);
  const [unassignedPunchesCountPercent, setUnassignedPunchesCountPercent] =
    useState<number | null>(null);

  useEffect(() => {
    const allPunchesCount = punches.length;

    const completedPunches: IPoint[] = punches.filter(
      (punch: IPoint) => punch.status === "Completed"
    );

    !completedPunches
      ? setCompletedPunchesCountPercent(0)
      : setCompletedPunchesCountPercent(
          Number(((completedPunches.length * 100) / allPunchesCount).toFixed(2))
        );

    const punchesInProgress: IPoint[] = punches.filter(
      (punch: IPoint) => punch.status === "In progress"
    );

    !punchesInProgress
      ? setInProgressPunchesCountPercent(0)
      : setInProgressPunchesCountPercent(
          Number(
            ((punchesInProgress.length * 100) / allPunchesCount).toFixed(2)
          )
        );

    const unassignedPunches: IPoint[] = punches.filter(
      (punch: IPoint) => punch.status === "Unassigned"
    );

    !unassignedPunches
      ? setUnassignedPunchesCountPercent(0)
      : setUnassignedPunchesCountPercent(
          Number(
            ((unassignedPunches.length * 100) / allPunchesCount).toFixed(2)
          )
        );
  });

  return (
    <>
      <h3 className="text-xl pb-5 font-bold">Quick Stats</h3>
      <div className="flex gap-20 sm:flex-col sm:text-center sm:items-center">
        <RadialProgress
          title="Completed punches"
          punchByStatusInPercent={completedPunchesCountPercent}
          color="text-green-500"
        />
        <RadialProgress
          title="Punches in progress"
          punchByStatusInPercent={inProgressPunchesCountPercent}
          text-secondary
          color="text-yellow-500"
        />
        <RadialProgress
          title="Unassigned punches"
          punchByStatusInPercent={unassignedPunchesCountPercent}
          color="text-red-500"
        />
      </div>
    </>
  );
};
