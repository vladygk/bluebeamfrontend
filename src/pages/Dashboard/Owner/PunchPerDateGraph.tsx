import { FC, useEffect, useState } from "react";
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
  Label,
  Legend,
} from "recharts";
import { IPoint } from "../../../components/Drawing/IPoint";

interface IGraph {
  punches: IPoint[];
}

export const PunchPerDateGraph: FC<IGraph> = ({ punches }) => {
  const [punchCounts, setPunchCounts] = useState<
    { date: number; count: number }[]
  >([]);

  useEffect(() => {
    // Function to count punches by date
    const countPunchesByDate = () => {
      const counts: { [date: number]: number } = {};

      for (const punch of punches) {
        const startDate = new Date(punch.startDate);
        const date = startDate.getDate();
        counts[date] = (counts[date] || 0) + 1;
      }

      const currentDate = new Date();
      // const totalDays = new Date(
      //       new Date().getFullYear(),
      //       new Date().getMonth() + 1,
      //       0
      //     ).getDate();
      const totalDays = currentDate.getDate();
      const punchCountsArr: { date: number; count: number }[] = Array.from(
        { length: totalDays },
        (_, index) => ({
          date: index + 1,
          count: counts[index + 1] || 0,
        })
      );

      setPunchCounts(punchCountsArr);
      console.log("punchcounts", punchCounts);
    };

    countPunchesByDate();
  }, [punches]);

  const punchCountsArray = punchCounts.map((punchCount) => ({
    date: punchCount.date,
    count: punchCount.count,
  }));

  const legendFormatter = (value: string) => {
    if (value === "count") {
      return "Punches Count";
    }
    return value;
  };

  return (
    <div className="w-[80%] sm:w-full">
      <h3 className="text-xl my-10 font-bold">Punches per Day</h3>
      <div className="w-full sm:w-full">
        <ResponsiveContainer width="100%" height={300}>
          <AreaChart
            data={punchCountsArray}
            margin={{ top: 0, right: 30, left: 0, bottom: 0 }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="date">
              <Label
                value="Day of current month"
                position="insideBottomRight"
                offset={-6}
              />
            </XAxis>
            <YAxis>
              <Label
                value="Punches"
                position="insideLeft"
                angle={-90}
                offset={10}
              />
            </YAxis>
            <Tooltip />
            <Legend formatter={legendFormatter} />
            <Area
              type="monotone"
              dataKey="count"
              stroke="#8884d8"
              fill="#8884d8"
            />
          </AreaChart>
        </ResponsiveContainer>
      </div>
    </div>
  );
};
