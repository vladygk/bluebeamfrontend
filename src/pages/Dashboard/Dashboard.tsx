import { useContext } from "react";
import { useParams } from "react-router-dom";

import { AuthContext } from "../../contexts";

import { FacilityInfo } from "../../components/FacilitySummarySection/FacilityInfo";
import { DashboardMaintainer } from "./Maintainer/DashboardMaintainer";
import { OwnerDashboard } from "./Owner/OwnerDashboard";
import { FMDashboard } from "./FM/FMDashboard";

export const Dashboard = () => {
  const { facilityId } = useParams();
  const { userRole } = useContext(AuthContext);

  return (
    <>
      <div className="flex flex-col items-center py-10 ">
        <FacilityInfo id={facilityId!} />
        {userRole === "Worker" && <DashboardMaintainer />}
        {userRole === "FM" && <FMDashboard />}
      </div>
      {userRole === "Owner" && <OwnerDashboard />}
    </>
  );
};
