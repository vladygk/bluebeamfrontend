import { useEffect, useState } from "react";
import { SimpleTable } from "../../../components/FacilitySummarySection/SimpleTable";
import { LatestSpaces } from "./LatestSpaces";
import {
  IFetchedSpace,
  getAllSpacesForFacility,
} from "../../../services/SpaceServices";
import { useParams } from "react-router-dom";

export const FMDashboard = () => {
  const { facilityId } = useParams();
  const [spaces, setSpaces] = useState<IFetchedSpace[]>([]);

  useEffect(() => {
    getAllSpacesForFacility(Number(facilityId)).then((f) => {
      if (f) {
        setSpaces(f);
      }
    });
  }, []);
  return (
    <>
      <LatestSpaces spaces={spaces.slice(0, 3)} />
      <SimpleTable name="punches" limit={5} />
      <SimpleTable name="assets" limit={5} />
    </>
  );
};
