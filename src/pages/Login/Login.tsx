import { useState, useContext, FC, useEffect } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../../contexts/";
import { ITokenPayload, decodeToken } from "../../utils/tokenUtils";
import { validate } from "../../utils/formValidation";
import { IHasError } from "../../utils/formValidation";

import { Input } from "../../components/Input/";

import login from "../../assets/photos/tasks-jobs.svg";
import { saveToStorage } from "../../utils/localStorage";

export interface IValueLogin {
  email: string;
  password: string;
}

export interface IValueLogin {
  email: string;
  password: string;
}

export const Login: FC = () => {
  const [value, setValue] = useState<IValueLogin>({ email: "", password: "" });
  const [error, setError] = useState<IHasError>({
    email: false,
    password: false,
  });
  const {
    onLogin,
    responseStatusError,
    setUserName,
    setUserId,
    setUserRole,
    setResponseStatusError,
  } = useContext(AuthContext);
  const [showPassword, setShowPassword] = useState<boolean>(false);

  const [isLogging, setIsLogging] = useState(false);

  useEffect(() => {
    setResponseStatusError("");
  }, []);

  const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) =>
    setValue((state) => ({ ...state, [e.target.name]: e.target.value }));

  const showPasswordHandler = (): void => {
    setShowPassword(!showPassword);
  };

  const handleLogin = async (e: React.FormEvent<Element>) => {
    e.preventDefault();

    setIsLogging(true);
    const userData = await onLogin(e, value, error);

    setIsLogging(false);

    if (!responseStatusError) {
      const decodeTokenData: ITokenPayload = await decodeToken();

      if (setUserName) {
        const name = decodeTokenData["name"];

        setUserName(name);
        saveToStorage("userName", name);
      }
      if (setUserId) {
        const id = decodeTokenData["nameidentifier"];

        setUserId(id);
        saveToStorage("userId", id);
      }
      if (setUserRole) {
        const role = decodeTokenData["role"]!;

        setUserRole(role);
        saveToStorage("userRole", role);
      }
    }
  };

  return (
    <>
      <div className="grid grid-cols-2 gap-10 py-24 px-20 md:grid-cols-1 md:py-20 md:px-10 md:gap-14 sm:py-10 sm:px-8">
        <div className=" flex items-center justify-center md:order-2 sm:hidden">
          <img
            className=" rounded-sm w-4/5 md:w-3/4 sm:w-full"
            src={login}
            alt="buildersPhoto"
          />
        </div>
        <div className="w-2/3 border-2 border-success px-7 py-5 mx-auto rounded-md sm:w-full">
          <form data-testid="form" onSubmit={handleLogin}>
            <h2 className="m-6 text-center text-2xl font-bold text-accent">
              Log in
            </h2>
            <Input
              labelText="Email:"
              type="text"
              placeholder="Enter Email"
              name="email"
              onChange={onChangeHandler}
              value={value["email"]}
              hasError={error["email"]}
              onBlur={() => validate(value["email"], "email", setError)}
            ></Input>
            <Input
              labelText="Password:"
              type={showPassword ? "text" : "password"}
              placeholder="Enter Password"
              name="password"
              onChange={onChangeHandler}
              value={value["password"]}
              hasError={error["password"]}
              onBlur={() => validate(value["password"], "password", setError)}
            ></Input>
            <div className="flex items-center mt-4">
              <input
                onClick={showPasswordHandler}
                type="checkbox"
                id="showPassword"
                className="mr-2 checkbox checkbox-secondary checkbox-sm"
              />
              <label htmlFor="showPassword" className="text-accent">
                Show Password
              </label>
            </div>
            <button className="btn btn-secondary w-full my-5">
              {isLogging ? "Logging in ..." : "Submit"}
            </button>
            {responseStatusError !== "" && (
              <span className=" text-error">{responseStatusError}</span>
            )}
            <p className="text-center text-accent">
              Forgot{" "}
              <a href="" className="text-blue-500">
                Password?
              </a>
            </p>
            <p className="text-center text-accent">
              Don't have an account?
              <Link className="text-blue-500 m-1" to="/register">
                Sign up
              </Link>
            </p>
          </form>
        </div>
      </div>
    </>
  );
};
