import { render } from "@testing-library/react";
import { Login } from ".";
import { MemoryRouter } from "react-router-dom";
import "@testing-library/jest-dom";

describe("Login test", () => {
    test("Test1", () => {
        const { getByTestId } = render(
            <MemoryRouter>
                <Login />
            </MemoryRouter>
        );

        expect(getByTestId("form")).toBeInTheDocument();
    });
});
