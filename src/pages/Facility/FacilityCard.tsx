import { FC, useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { IFetchedFacility } from "../../services/FacilityServices";
import { AuthContext } from "../../contexts";
import { GetAllForMaintainer } from "../../services/punch";

import { PlaceholderImage } from "../../components/PlaceholderImage/PlaceholderImage";

export const FacilityCard: FC<IFetchedFacility> = (props: IFetchedFacility) => {
  const { userRole, userId } = useContext(AuthContext);
  const [punchCount, setPunchCount] = useState(0);
  const [imageLoaded, setImageLoaded] = useState(false);

  const handleImageLoad = () => {
    setImageLoaded(true);
  };

  useEffect(() => {
    let punchData;
    let count: number;
    const getPunchCount = async () => {
      if (userRole === "Worker") {
        punchData = await GetAllForMaintainer(Number(userId), Number(props.id));
        count = punchData.filter((x: any) => x.status !== "Completed").length;
      }
      if (userRole === "FM") {
        punchData = await GetAllForMaintainer(null, props.id, undefined, true);
        count = punchData.length;
      }
      setPunchCount(count);
    };

    getPunchCount();
  }, []);
  return (
    <div className=" indicator hover:scale-[1.05] hover:cursor-pointer transition duration-300 w-full h-full">
      {punchCount && punchCount != 0 ? (
        <span className="indicator-item badge badge-error text-primary rounded-full px-2 py-3">
          {punchCount}
        </span>
      ) : null}
      <Link
        to={`/facility/${props.id}`}
        className="flex flex-col justify-start w-[100%]"
      >
        <div className=" w-full h-full ">
          {!imageLoaded && <PlaceholderImage />}
          <img
            className="border-2 rounded-md border-[gray] w-[100%] h-[350px] lg:h-[250px]"
            style={{ display: imageLoaded ? "block" : "none" }}
            src={props.imageUrl}
            alt={props.name}
            onLoad={handleImageLoad}
          />
          <p className="text-center text-accent">
            {props.name}, {props.city}, {props.address}
          </p>
        </div>
      </Link>
    </div>
  );
};
