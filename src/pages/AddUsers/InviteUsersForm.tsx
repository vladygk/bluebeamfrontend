import {
  ChangeEvent,
  Dispatch,
  FC,
  useContext,
  useEffect,
  useState,
} from "react";
import { Input } from "../../components/Input/Input";
import { useNavigate, useParams } from "react-router-dom";
import { IErrorCriteria, validationCriteria } from "../../utils/formValidation";
import emailjs from "@emailjs/browser";
import { addUserToFacility, getOneUser } from "../../services/auth";
import {
  IFetchedFacility,
  getOneFacility,
} from "../../services/FacilityServices";
import { AuthContext } from "../../contexts";
import * as CryptoJS from "crypto-js";
import { encryptionKey } from "../../utils/common.ts";
export interface IValueInvite {
  name: string;
  email: string;
  role: string;
  facility: string | undefined;
}

interface IEmailHasError {
  email: boolean;
}

export const InviteUsersForm: FC = () => {
  const navigate = useNavigate();
  const { facilityId } = useParams();
  const { userName } = useContext(AuthContext);

  const [facility, setFacility] = useState<IFetchedFacility>();

  const [error, setError] = useState<IEmailHasError>({
    email: false,
  });

  const [formValues, setFormValues] = useState<IValueInvite>({
    name: "",
    email: "",
    role: "2",
    facility: facilityId,
  });

  const validate = (
    value: string,
    key: string,
    setError: Dispatch<React.SetStateAction<IEmailHasError>>
  ) => {
    if (!value.match(validationCriteria[key as keyof IErrorCriteria])) {
      setError((state) => ({ ...state, [key]: true }));
    } else {
      setError((state) => ({ ...state, [key]: false }));
    }
  };

  const onChangeHandler = (e: ChangeEvent<HTMLInputElement>) =>
    setFormValues((state) => ({ ...state, [e.target.name]: e.target.value }));

  const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectOption = e.target.value;

    setFormValues((prevState) => ({
      ...prevState,
      [e.target.name]: selectOption,
    }));
  };

  useEffect(() => {
    getOneFacility(Number(facilityId))
      .then((f) => {
        if (f !== undefined) {
          setFacility(f);
        }
      })
      .catch((e) => navigate("/error"));
  }, []);

  const handleSendInvite = async (event: React.FormEvent) => {
    event.preventDefault();

    const user = await getOneUser(formValues.email);

    if (user.id) {
      await addUserToFacility(Number(user.id), Number(facilityId));
    } else {
      const hiddenForm = document.createElement("form");
      hiddenForm.style.display = "none";
      hiddenForm.action = "YOUR_FORM_ACTION_URL";
      hiddenForm.method = "POST";

      const createAndAppendInput = (name: string, value: string) => {
        const input = document.createElement("input");
        input.type = "hidden";
        input.name = name;
        input.value = value;
        hiddenForm.appendChild(input);
      };

      createAndAppendInput("fmName", userName);
      createAndAppendInput("facilityName", facility!.name);
      createAndAppendInput("recieverName", formValues.name);
      createAndAppendInput(
        "recieverRole",
        formValues.role == "2" ? "owner" : "worker"
      );
      const queryParams = `email=${formValues.email}&role=${formValues.role}&facilityId=${facilityId}`;
      //const testParams = `email=dmoni@abv.bg&role=2&facilityId=3`;

      const encodedQueryParams = CryptoJS.AES.encrypt(
        JSON.stringify(queryParams),
        encryptionKey,
        { mode: CryptoJS.mode.ECB }
      ).toString();
      console.log(encodedQueryParams);

      //console.log(encodedQueryParams);
      //console.log("key:", encryptionKey.toString());
      const message = `Please click on the following link to register: http://${window.location.host}/register?encrypted=${encodedQueryParams}`;

      createAndAppendInput("message", message);
      createAndAppendInput("recieverEmail", formValues.email);

      document.body.appendChild(hiddenForm);

      emailjs
        .sendForm(
          "service_ux6tnnn",
          "template_azfjgdl",
          hiddenForm,
          "bzwcvvJ6FvLLT-NYK"
        )
        .then(
          (result) => {
            console.log(result.text);
          },
          (error) => {
            console.log(error.text);
          }
        );
    }

    navigate(`/facility/${facilityId}/users`);
  };

  return (
    <div className="border w-[30rem] md:w-[70%] sm:w-[100%] rounded-md">
      <form className="py-10 px-10 sm:py-5">
        <h2 className="text-2xl"></h2>
        <Input
          labelText="Name"
          name="name"
          type="text"
          placeholder="User name"
          onChange={onChangeHandler}
        ></Input>
        <Input
          labelText="Email:"
          type="text"
          placeholder="Enter Email"
          name="email"
          onChange={onChangeHandler}
          value={formValues["email"]}
          hasError={error["email"]}
          onBlur={() => validate(formValues["email"], "email", setError)}
        ></Input>
        <label htmlFor="role" className="font-bold text-accent">
          Role:
        </label>
        <select
          onChange={handleSelectChange}
          value={formValues.role}
          className="border-2 w-full rounded-md h-10 pl-2 bg-primary text-accent"
          id="role"
          name="role"
        >
          <option key="2" value="2">
            Owner
          </option>
          <option key="3" value="3">
            Worker
          </option>
        </select>
        <button className="btn btn-secondary mt-10" onClick={handleSendInvite}>
          Send Invite
        </button>
      </form>
    </div>
  );
};
