import { FC } from "react";
import { InviteUsersForm } from "./InviteUsersForm";

import send from "../../assets/photos/send.svg";

export const AddUsers: FC = () => {
  return (
    <div className="w-[90%] mx-16 my-8 min-h-[calc(100svh-88px)] max-w-[calc(100svw-8.5rem)] md:w-[95%] md:ml-8 mr-0">
      <h2 className="text-3xl text-accent text-bold mb-20 md:text-center">
        Invite User
      </h2>
      <div className="flex justify-evenly md:flex-col-reverse md:gap-y-10 md:items-center">
        <img alt="Send image" src={send} className="w-[15rem]" />
        <InviteUsersForm />
      </div>
    </div>
  );
};
