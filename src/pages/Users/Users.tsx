import React, { useContext } from "react";
import { UsersTable } from "../../components/UsersTable";
import { Link } from "react-router-dom";
import { AuthContext } from "../../contexts";

export const Users = () => {
  const { userRole } = useContext(AuthContext);

  return (
    <div className="w-[90%] mx-16 my-8 min-h-[calc(100svh-88px)] max-w-[calc(100svw-10.5rem)] md:w-[95%] md:mx-12 sm:mx-0 sm:px-5 sm:mb-5 sm:w-full sm:max-w-full ">
      <div className="flex justify-between sm:flex-col sm:text-center sm:items-center">
        <h2 className="text-3xl text-accent text-bold mb-16 sm:mb-10">
          Facility's users
        </h2>
        {userRole == "FM" && (
          <Link
            to="./add"
            className="btn btn-secondary text-lg sm:w-[10rem] sm:mb-10"
          >
            Add User
          </Link>
        )}
      </div>
      <UsersTable />
    </div>
  );
};
