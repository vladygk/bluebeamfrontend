// import { render } from "@testing-library/react";
// import { Home } from ".";
// import { AuthContext, IAuthContext } from "../../contexts/AuthContext";
// import { BrowserRouter as Router } from "react-router-dom";

// import "@testing-library/jest-dom";

// describe("home page functionality", () => {
//   test("display task section when user is logged in", () => {
//     const context: IAuthContext = {
//       userData: { email: "", accessToken: "" },
//       onRegister: (e, value, repeatPassword) => {
//         return new Promise(() => true);
//       },
//       onLogin: (e, value) => {
//         return new Promise(() => true);
//       },
//       onLogout: () => {
//         return new Promise(() => true);
//       },
//     };

//     const { getByTestId } = render(
//       <>
//         <AuthContext.Provider value={context}>
//           <Router>
//             <Home />
//           </Router>
//         </AuthContext.Provider>
//       </>
//     );

//     const divElement = getByTestId("task-section");

//     expect(divElement).toBeInTheDocument();
//   });
// });
