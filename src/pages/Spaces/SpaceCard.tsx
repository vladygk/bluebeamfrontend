import { FC, useContext, useState } from "react";
import { Link } from "react-router-dom";
import { IFetchedSpace } from "../../services/SpaceServices";
import { ThemeContext } from "../../contexts";
import { PlaceholderImage } from "../../components/PlaceholderImage/PlaceholderImage";

export const SpaceCard: FC<IFetchedSpace> = (props: IFetchedSpace) => {
  const { theme } = useContext(ThemeContext);

  const [imageLoaded, setImageLoaded] = useState(false);

  const handleImageLoad = () => {
    setImageLoaded(true);
  };
  return (
    <Link to={`${props.id}`} className="flex flex-col justify-start w-[100%]">
      <div className="hover:scale-[1.05] w-[100%] h-[100%] hover:cursor-pointer transition duration-300">
        {!imageLoaded && <PlaceholderImage />}
        <img
          className={`border-2 rounded-md border-[gray] w-[100%] h-[350px] lg:h-[250px] ${
            theme == "darkTheme" && "dark-img"
          }`}
          style={{ display: imageLoaded ? "block" : "none" }}
          src={props.imageUrl}
          alt={props.name}
          onLoad={handleImageLoad}
        />
        <p className="text-center text-accent">
          {props.name}, {props.location}
        </p>
      </div>
    </Link>
  );
};
