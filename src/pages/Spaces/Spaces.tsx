import { FC, useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { SpaceCard } from "./SpaceCard";
import {
  getAllSpaces,
  getAllSpacesForFacility,
  IFetchedSpace,
} from "../../services/SpaceServices";
import { getOneFacility } from "../../services/FacilityServices";
import { getRoleFromStorage } from "../../utils/localStorage";

export const Spaces: FC = () => {
  const [spaces, setSpaces] = useState<IFetchedSpace[]>([]);
  const { facilityId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    getOneFacility(Number(facilityId)).catch((e) => navigate("/error"));
    getAllSpacesForFacility(Number(facilityId)).then((f) => {
      if (f !== undefined) {
        setSpaces(f);
      }
    });
  }, []);

  return (
    <div className="px-24 py-10 md:px-14 sm:px-10 min-h-screen sm:w-full">
      <div className="flex justify-between items-center pb-10 rounded-md sm:flex-col">
        <h1 className="text-left my-[10px] text-accent text-[3rem] font-bold">
          Spaces
        </h1>
        {getRoleFromStorage().toLowerCase() === "fm" && (
          <Link
            to={`/facility/${Number(facilityId)}/space/add`}
            className="btn btn-success text-lg"
          >
            Add space{" "}
          </Link>
        )}
      </div>

      <div className="grid grid-cols-3 gap-[3rem] md:grid-cols-2 md:gap-[2rem] sm:grid-cols-1 sm:gap-[1rem]">
        {spaces.map((element: any) => (
          <SpaceCard
            id={element.id}
            key={element.id}
            name={element.name}
            location={element.location}
            imageUrl={element.imageUrl}
            facilityId={element.facilityId}
          />
        ))}
      </div>
    </div>
  );
};
