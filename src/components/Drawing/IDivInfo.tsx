export interface IDivInfo {
  name: string;
  status: string;
  assignee: string;
  date: string;
  display: string;
  x: number;
  y: number;
}
