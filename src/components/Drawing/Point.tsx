import React, { useEffect, useState, useContext } from "react";
import { IPoint } from "./IPoint";
import { InfoDiv } from "./InfoDiv";
import { useParams } from "react-router-dom";
import { SpaceContext } from "../../contexts/ISpaceContext";

export const Point = ({
  id,
  coordX: coordX,
  coordY: coordY,
  status,
  title,
  assignee,
  assetId,
  asset,
  expirationDate,
  startDate,
  description,
  creatorId,
  assigneeId,
  statusId,
  isClicked,
}: IPoint) => {
  const params = useParams();
  const [pointColor, setPointColor] = useState("");
  const [hoverDivClassName, setHoverDivClassName] =
    useState<string>("hiddenDiv");

  const { isActiveForm, setIsActiveForm, setClickedPointData, setButtonType } =
    useContext(SpaceContext)!;

  useEffect(() => {
    if (!isActiveForm) {
      setClickedPointData({
        id: 70000,
        title: "",
        description: "",
        assigneeId: null,
        assignee: null, // Name
        assetId: null,
        asset: "",
        expirationDate: "",
        startDate: "",
        spaceId: "",
        creatorId: "1",
        status: "Unassinged",
        coordX: -1,
        coordY: -1,
        statusId: -1,
        isClicked: false,
      });
    }
  }, [isActiveForm]);

  useEffect(() => {
    switch (status) {
      case "Unassigned":
        setPointColor("red");
        break;
      case "In progress":
        setPointColor("yellow");
        break;
      case "Completed":
        setPointColor("green");
        break;
      default:
        setPointColor("black");
    }
  }, [status]);

  const handleMouseHover = (): void => {
    setHoverDivClassName("visibleDiv");
  };

  const handleMouseLeave = (): void => {
    setHoverDivClassName("hiddenDiv");
  };

  const handlePointClick = (event: React.MouseEvent<HTMLElement>) => {
    if (isActiveForm) return;

    setButtonType("details");
    setIsActiveForm(() => !isActiveForm);

    setClickedPointData({
      id: id,
      title: title,
      description: description,
      assignee: assignee,
      assigneeId: assigneeId,
      assetId: assetId,
      asset: asset,
      startDate: startDate,
      expirationDate: expirationDate,
      spaceId: params["id"]!,
      status: status,
      coordX: coordX,
      coordY: coordY,
      creatorId: creatorId,
      statusId: statusId,
      isClicked: isClicked,
    });
  };

  return (
    <>
      <InfoDiv
        display={hoverDivClassName}
        name={title}
        assignee={assignee!}
        status={status}
        date={expirationDate}
        x={coordX}
        y={coordY}
      />

      <div
        onMouseEnter={handleMouseHover}
        onMouseLeave={handleMouseLeave}
        onClick={handlePointClick}
        key={id}
        className={` point ${isClicked && "active-point"} `}
        style={{
          left: `${coordX}%`,
          top: `${coordY}%`,
          width: `${
            isClicked
              ? window.innerWidth * 0.017 + 14
              : window.innerWidth * 0.017
          }px`,
          height: `${
            isClicked
              ? window.innerWidth * 0.017 + 14
              : window.innerWidth * 0.017
          }px`,
          backgroundColor: pointColor,
        }}
      ></div>
    </>
  );
};
