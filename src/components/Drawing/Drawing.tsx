import React, { FC, useContext, useEffect, useState } from "react";
import "./drawing.css";
import { IPoint } from "./IPoint";
import { Point } from "./Point";
import { useNavigate, useParams } from "react-router-dom";
import { IFetchedSpace, getOneSpace } from "../../services/SpaceServices";
import { ThemeContext } from "../../contexts";
import { SpaceContext } from "../../contexts/ISpaceContext";

export const Drawing: FC = () => {
  interface IDrawingDivSize {
    width: string;
    height: string;
  }

  const {
    isActiveForm,
    setIsActiveForm,
    isDrawable,
    setIsDrawable,
    points,
    setPoints,
    clickedPointData,
    setClickedPointData,
  } = useContext(SpaceContext)!;

  const { facilityId, spaceId } = useParams();
  const navigate = useNavigate();

  const [pointCount, setPointCount] = useState(0);
  const [space, setSpace] = useState<IFetchedSpace>({
    id: -1,
    name: "",
    location: "",
    imageUrl: "",
    facilityId: -1,
  });

  const [divSizes, setDivSizes] = useState<IDrawingDivSize>({
    width: `${window.innerWidth * 0.7}px`,
    height: `${window.innerWidth * 0.7 * 0.5}px`,
  });

  const [pointSize, setPointSize] = useState({
    width: `${window.innerWidth * 0.017}px`,
    height: `${window.innerWidth * 0.017}px`,
  });
  const { theme } = useContext(ThemeContext);

  useEffect(() => {
    if (isActiveForm) {
      setDivSizes({
        width: `${window.innerWidth * 0.5}px`,
        height: `${window.innerWidth * 0.5 * 0.5}px`,
      });
    } else {
      setDivSizes({
        width: `${window.innerWidth * 0.7}px`,
        height: `${window.innerWidth * 0.7 * 0.5}px`,
      });
    }
  }, [isActiveForm]);

  useEffect(() => {
    getOneSpace(Number(spaceId), Number(facilityId))
      .then((f) => {
        if (f !== undefined) {
          setSpace(f);
        }
      })
      .catch((e) => navigate("/error"));
  }, []);

  useEffect(() => {
    return () => {
      setClickedPointData({
        id: 77,
        title: "",
        description: "",
        assigneeId: null,
        assignee: null, // Name
        assetId: null,
        asset: "",
        expirationDate: "",
        startDate: "",
        spaceId: "",
        creatorId: "1",
        status: "Unassinged",
        coordX: -1,
        coordY: -1,
        statusId: -1,
        isClicked: false,
      });
      setIsActiveForm(false);
    };
  }, []);

  const getCurrentDate = (): string => {
    const date = new Date();

    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();

    const currentDate = `${year}-${Number(month) > 9 ? month : "0" + month}-${
      Number(day) > 9 ? day : "0" + day
    }`;
    console.log(currentDate);

    return currentDate;
  };

  const handleDivClick = (event: React.MouseEvent<HTMLElement>): void => {
    if (!isDrawable) return;

    const { offsetX, offsetY } = event.nativeEvent;
    const { width, height } = calculateClickWindowPercentage(offsetX, offsetY);

    const newPoint: IPoint = {
      id: pointCount,
      coordX: width,
      coordY: height,
      status: "Unassigned",
      size: pointSize,
      title: "",
      assignee: "",
      assigneeId: -1,
      assetId: null,
      asset: "",
      expirationDate: "",
      startDate: getCurrentDate(),
      description: "",
      creatorId: "",
      spaceId: "",
      statusId: -1,
      isClicked: false,
    };

    const clickedElementId = (event.target as HTMLInputElement).id;
    if (clickedElementId == "drawing-div") {
      setPoints((prev) => [...prev, newPoint]);
      setPointCount((prev) => prev + 1);
    }
    setIsActiveForm(true);
    setIsDrawable(false);
  };

  const calculateClickWindowPercentage = (
    x: number,
    y: number
  ): { width: number; height: number } => {
    const width = isActiveForm
      ? window.innerWidth * 0.5
      : window.innerWidth * 0.7;
    const height = isActiveForm
      ? window.innerWidth * 0.5 * 0.5
      : window.innerWidth * 0.7 * 0.5;
    return {
      width: (x / width) * 100,
      height: (y / height) * 100,
    };
  };

  const handleWindowResize = (): void => {
    const div = document.getElementById("main-div");
    const points = document.getElementsByClassName(
      "point"
    ) as HTMLCollectionOf<HTMLElement>;

    if (div) {
      div.style.width = `${
        isActiveForm ? window.innerWidth * 0.5 : window.innerWidth * 0.7
      }px`;
      div.style.height = `${
        Number(div.style.width.substring(0, div.style.width.length - 2)) / 2
      }px`;
    }
    Array.from(points).forEach((element) => {
      element.style.width = `${window.innerWidth * 0.017}px`;
      element.style.height = `${window.innerWidth * 0.017}px`;
    });
  };

  window.addEventListener("resize", handleWindowResize);

  return (
    <div
      id="main-div"
      className={`mx-auto relative box-border ${
        isDrawable ? "hover:cursor-pointer" : ""
      }`}
      style={divSizes}
    >
      <img
        className={`w-full h-full ${theme === "darkTheme" && "dark-img"}`}
        src={space.imageUrl}
        alt="room-image"
      />
      <div
        id="drawing-div"
        className={`rounded-md absolute top-0 z-10 box-border w-full h-full border-[5px] ${
          isDrawable ? "border-secondary" : "border-base-200"
        }`}
        onClick={handleDivClick}
      >
        {points.map((point, index) => (
          <Point
            key={index}
            id={point.id}
            coordX={point.coordX}
            coordY={point.coordY}
            status={point.status}
            size={pointSize}
            title={point.title}
            assignee={point.assignee}
            assigneeId={point.assigneeId}
            assetId={point.assetId}
            asset={point.asset}
            expirationDate={point.expirationDate}
            startDate={point.startDate}
            description={point.description}
            creatorId={point.creatorId}
            spaceId={point.spaceId}
            statusId={point.statusId}
            isClicked={clickedPointData.id == point.id}
          />
        ))}
      </div>
    </div>
  );
};
