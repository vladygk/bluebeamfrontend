import { FC, useEffect, useState } from "react";
import { IDivInfo } from "./IDivInfo";

export const InfoDiv: FC<IDivInfo> = ({
  name,
  status,
  assignee,
  date,
  display,
  x,
  y,
}) => {
  const [displayType, setDisplayType] = useState<string>("hiddenDiv");

  useEffect(() => {
    setDisplayType(display);
  }, [display]);

  return (
    <div
      className={`bg-primary text-accent info-div ${displayType}`}
      style={{ left: `calc(${x + 2}% )`, top: `${y + 2}%` }}
    >
      <label className="font-bold">Name:</label>
      <p>{name}</p>
      <label className="font-bold">Status:</label>
      <p>{status} </p>
      <label className="font-bold">Assignee:</label>
      <p>{assignee}</p>
      <label className="font-bold">Date:</label>
      <p>{date.split(" ")[0]}</p>
    </div>
  );
};
