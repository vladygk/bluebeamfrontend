import { FC, useContext, useEffect, useState } from "react";
import { Link, useLocation, useParams } from "react-router-dom";
import {
  RiBuildingFill,
  RiAspectRatioFill,
  RiAlbumFill,
  RiKeynoteFill,
  RiTeamFill,
} from "react-icons/ri";
import { AuthContext } from "../../contexts";

export const SideBar: FC = () => {
  const [isSmallScreen, setIsSmallScreen] = useState(false);
  const location = useLocation();
  const { facilityId } = useParams();
  const { userRole } = useContext(AuthContext)!;

  useEffect(() => {
    const mediaQuery = window.matchMedia("(max-width: 912px)");
    setIsSmallScreen(mediaQuery.matches);

    const handleMediaQueryChange = (e: MediaQueryListEvent) =>
      setIsSmallScreen(e.matches);
    mediaQuery.addEventListener("change", handleMediaQueryChange);

    // Remove event listener on unmount
    return () => {
      mediaQuery.removeEventListener("change", handleMediaQueryChange);
    };
  }, []);

  const linkDefaultClasses = "btn btn-secondary text-lg w-full mb-3";

  const linkActiveClasses = "btn btn-primary text-lg w-full mb-3";

  return (
    <div className="flex justify-center bg-info min-w-[13rem] md:min-w-[4.25rem] md:p-2">
      <ul className="sticky top-[100px] h-[200px] p-7 md:p-0">
        <li>
          <Link
            className={
              location.pathname === `/facility/${facilityId}`
                ? linkActiveClasses
                : linkDefaultClasses
            }
            to={`/facility/${facilityId}`}
          >
            {isSmallScreen ? (
              <div className="text-lg">
                <RiBuildingFill />
              </div>
            ) : (
              <div className="flex items-center gap-1 flex-grow">
                <RiBuildingFill />
                <p>Dashboard</p>
              </div>
            )}
          </Link>
        </li>
        <li>
          <Link
            className={
              location.pathname.includes("space")
                ? linkActiveClasses
                : linkDefaultClasses
            }
            to={`facility/${facilityId}/spaces`}
          >
            {isSmallScreen ? (
              <div className="text-lg">
                <RiAspectRatioFill />
              </div>
            ) : (
              <div className="flex items-center gap-1 flex-grow">
                <RiAspectRatioFill />
                <p>Spaces</p>
              </div>
            )}
          </Link>
        </li>
        {userRole !== "Worker" && (
          <>
            <li>
              <Link
                className={
                  location.pathname.includes("punch")
                    ? linkActiveClasses
                    : linkDefaultClasses
                }
                to={`/facility/${facilityId}/punches`}
              >
                {isSmallScreen ? (
                  <div className="text-lg">
                    <RiAlbumFill />
                  </div>
                ) : (
                  <div className="flex items-center gap-1 flex-grow">
                    <RiAlbumFill />
                    <p>Punches</p>
                  </div>
                )}
              </Link>
            </li>
            <li>
              <Link
                className={
                  location.pathname.includes("asset")
                    ? linkActiveClasses
                    : linkDefaultClasses
                }
                to={`/facility/${facilityId}/assets`}
              >
                {isSmallScreen ? (
                  <div className="text-lg">
                    <RiKeynoteFill />
                  </div>
                ) : (
                  <div className="flex items-center gap-1 flex-grow">
                    <RiKeynoteFill />
                    <p>Assets</p>
                  </div>
                )}
              </Link>
            </li>
            <li>
              <Link
                className={
                  location.pathname.includes("users")
                    ? linkActiveClasses
                    : linkDefaultClasses
                }
                to={`/facility/${facilityId}/users`}
              >
                {isSmallScreen ? (
                  <div className="text-lg">
                    <RiTeamFill />
                  </div>
                ) : (
                  <div className="flex items-center gap-1 flex-grow">
                    <RiTeamFill />
                    <p>Users</p>
                  </div>
                )}
              </Link>
            </li>
          </>
        )}
      </ul>
    </div>
  );
};
