import { FC, useEffect, useState } from "react";
import { Outlet, useLocation } from "react-router-dom";
import { NavigationBar } from "../NavigationBar/NavigationBar";
import { Footer } from "../Footer/Footer";
import { SideBar } from "../SideBar";
import locations from "./sideBarLocations.json";

export const Layout: FC = () => {
  const [currLocation, setCurrLocation] = useState("");

  const location = useLocation().pathname.split("/")[1].toLocaleLowerCase();

  useEffect(() => {
    setCurrLocation(location);
  }, [location]);
  return (
    <>
      <header className="h-[88px]">
        <NavigationBar />
      </header>
      <div className="flex min-h-[calc(100svh-158px)]">
        {/* 88px (header) + 70 px (footer) = 158px*/}
        {locations.includes(currLocation) && <SideBar />}
        <main className=" w-full m-auto overflow-x-auto">
          <Outlet />
        </main>
      </div>
      <Footer />
    </>
  );
};
