import { FC, useContext, useEffect, useState } from "react";
import { AuthContext, ThemeContext } from "../../contexts";
import { Link, useNavigate, useParams } from "react-router-dom";
import { IPoint } from "../Drawing/IPoint";
import {
  IFetchedAsset,
  getAllAssetsForFacility,
  removeAsset,
} from "../../services/asset";
import {
  GetAllForMaintainer,
  GetOne,
  getAllForSpace,
  remove,
} from "../../services/punch";
import { SpaceContext } from "../../contexts/ISpaceContext";
import { GetAllForFacility } from "../../services/punch";

interface ItemListProps {
  items: IFetchedAsset[] | IPoint[];
  setItems: any;
  name: string;
  title: string;
  limit?: number;
}

export const ItemList: FC<ItemListProps> = ({
  items,
  setItems,
  name,
  title,
  limit,
}) => {
  const { theme } = useContext(ThemeContext);
  const { facilityId } = useParams();
  const { setActiveButton } = useContext(SpaceContext)!;
  const navigate = useNavigate();

  const handlePunchDetails = async (id: number) => {
    const clickedPunch = await GetOne(id);
    navigate(`/facility/${facilityId}/spaces/${clickedPunch?.spaceId}`, {
      state: clickedPunch,
    });
    setActiveButton("space");
  };

  const handlePunchDelete = async (id: number, e: any) => {
    e.stopPropagation();
    await remove(id);
    setItems((prevPunches: any) =>
      prevPunches.filter((punch: any) => punch.id !== id)
    );
  };

  const handleAssetDelete = async (id: number) => {
    await removeAsset(id, Number(facilityId));
    setItems((prevAssets: any) =>
      prevAssets.filter((asset: any) => asset.id !== id)
    );
  };

  const slicedItems = limit ? items.slice(-limit).reverse() : items;

  return (
    <div
      className={`border-3 bg-info rounded-3xl ${
        limit ? "" : "max-h-[50vh]"
      } w-[60%] p-2 flex flex-col md:w-[90%] mb-10`}
    >
      <div className="latest-punches-header items-center justify-between px-6 flex">
        <h3 className="text-2xl text-accent py-4 font-bold self-center flex">
          {title}
        </h3>
        <Link
          to={`/facility/${facilityId}/${name}`}
          className="btn btn-outline btn-secondary ml-5"
        >
          View all
        </Link>
      </div>
      {items.length ? (
        <ul className={limit ? "" : "overflow-y-auto"}>
          {slicedItems.map((item) => (
            <li
              onClick={
                name == "punches"
                  ? () => handlePunchDetails(item.id)
                  : () => console.log("")
              }
              key={item.id}
              className={`text-accent mx-2 py-2 my-2 rounded-xl flex items-center justify-between pl-20 md:pl-5 bg-primary hover:bg-secondary
                  hover:text-white hover:cursor-pointer ${
                    name == "punches" ? "hover:cursor-pointer" : ""
                  }`}
            >
              {"name" in item && (
                <p className="md:pb-3 md:content-center md:pr-1">
                  {(item as IFetchedAsset).name}
                </p>
              )}

              {"title" in item && (
                <p className="md:pb-3 md:content-center md:pr-1">
                  {(item as IPoint).title}, {(item as IPoint).status}
                </p>
              )}
              <div>
                <button
                  className="btn btn-outline btn-error px-5 mx-5 hover:btn-error"
                  onClick={
                    name == "punches"
                      ? (e: any) => handlePunchDelete(item.id, e)
                      : () => handleAssetDelete(item.id)
                  }
                >
                  Delete
                </button>
              </div>
            </li>
          ))}
        </ul>
      ) : (
        <div className="flex items-center justify-center p-5">
          <p className="text-2xl">No {name}.</p>
        </div>
      )}
    </div>
  );
};

interface FacilitySummaryProps {
  name: string;
  limit?: number;
}

export const SimpleTable: FC<FacilitySummaryProps> = ({ name, limit }) => {
  const [punches, setPunches] = useState<IPoint[]>([]);
  const [assets, setAssets] = useState<IFetchedAsset[]>([]);
  const { userRole, userId } = useContext(AuthContext);

  const { facilityId, spaceId } = useParams();

  useEffect(() => {
    if (name === "punches") {
      if (spaceId) {
        getAllForSpace(Number(spaceId)).then((f) => {
          if (f !== undefined) {
            setPunches(f);
          }
        });
      } else {
        if (userRole === "Worker") {
          GetAllForMaintainer(
            Number(userId),
            Number(facilityId),
            Number(spaceId),
            false
          ).then((f) => {
            setPunches(f);
          });
        } else {
          GetAllForFacility(Number(facilityId)).then((f) => {
            if (f !== undefined) {
              setPunches(f);
            }
          });
        }
      }
    } else {
      getAllAssetsForFacility(Number(facilityId)).then((f) => {
        if (f !== undefined) {
          setAssets(f);
        }
      });
    }
  }, []);

  return (
    <>
      {name === "punches" && (
        <ItemList
          items={punches}
          setItems={setPunches}
          title="Latest punches"
          name="punches"
          limit={limit}
        />
      )}
      {name === "assets" && (
        <ItemList
          items={assets}
          setItems={setAssets}
          title="Latest assets"
          name="assets"
          limit={limit}
        />
      )}
    </>
  );
};
