import { FC, useEffect, useState } from "react";
import {
  IFetchedFacility,
  getOneFacility,
} from "../../services/FacilityServices";
import { useNavigate } from "react-router-dom";

interface IFacilityInfo {
  id: string | undefined;
}

export const FacilityInfo: FC<IFacilityInfo> = ({ id }) => {
  const [facility, setFacility] = useState<IFetchedFacility>({
    id: -1,
    name: "",
    city: "",
    address: "",
    imageUrl: "",
    owners: [],
  });
  const navigate = useNavigate();

  useEffect(() => {
    getOneFacility(Number(id))
      .then((f) => {
        if (f !== undefined) {
          setFacility(f);
        }
      })
      .catch((e) => navigate("/error"));
  }, []);

  return (
    <div className="text-4xl w-[60%] text-center mb-4">
      <h1 className="font-bold text-accent mb-2">{facility.name}</h1>
      <div className="flex flex-col text-2xl  items-center text-accent rounded-md p-7">
        <div className="flex flex-col">
          <h3 className="flex items-center my-1 rounded px-2 sm:flex-col">
            <span className="badge badge-lg badge-success w-[5rem] p-3">
              Address
            </span>
            <span className="mx-4 text-lg sm:w-[10rem]">
              {facility.city}, {facility.address}
            </span>
          </h3>
          <h3 className="flex items-center my-1 rounded px-2 sm:flex-col">
            <span className="badge badge-lg badge-success w-[5rem] p-3">
              Owners
            </span>
            <span className="mx-4 text-lg sm:w-[10rem]">
              {facility.owners?.join(", ")}
            </span>
          </h3>
        </div>
      </div>
    </div>
  );
};
