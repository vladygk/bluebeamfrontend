import { FC, MouseEventHandler } from "react";

interface IButton {
  customClass: string;
  text: string;
  onClick: MouseEventHandler<HTMLButtonElement>;
}

export const Button: FC<IButton> = ({ customClass, text, onClick }) => {
  const defaultClass =
    "h-14 text-primary bg-neutral hover:bg-secondary focus:t font-medium rounded-lg text-sm px-5 py-2.5 mr-2";

  return (
    <button
      onClick={onClick}
      type="button"
      className={defaultClass + " " + customClass}
    >
      {text}
    </button>
  );
};
