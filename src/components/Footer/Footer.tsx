import { FC, useContext } from "react";
import { ThemeContext } from "../../contexts/ThemeContext";
import { Button } from "../Button/Button";

export const Footer: FC = () => {
  const year = new Date().getFullYear();
  const { toggleTheme, theme } = useContext(ThemeContext);
  return (
    <footer className="h-[70px] w-full bottom-0 px-20 bg-neutral text-center flex justify-between items-center sm:px-5">
      <p className="text-primary text-left">
        © {year} BlueBean, All Rights Reserved
      </p>
      <div className="">
        <Button
          onClick={toggleTheme}
          text={theme === "darkTheme" ? "Light mode" : "Dark mode"}
          customClass=""
        />
      </div>
    </footer>
  );
};
