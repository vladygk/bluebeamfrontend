import { useContext, useEffect, useState } from "react";
import { Input } from "../Input";
import { Link, useNavigate, useParams } from "react-router-dom";
import { ThemeContext } from "../../contexts/ThemeContext";
import { RiLinksLine } from "react-icons/ri";
import convertFileToString from "../../utils/convertFileToString";
import { createFacility } from "../../services/FacilityServices";

export interface IValueFacilityForm {
  name: string;
  city: string;
  address: string;
  photo: File | null;
  owner: string;
}

export interface IValueFacilityBody {
  Name: string;
  City: string;
  Address: string;
  Owner: string;
  ImageUrl: string;
}

export const FacilityForm = () => {
  const [formState, setFormState] = useState<IValueFacilityForm>({
    name: "",
    city: "",
    address: "",
    photo: null,
    owner: "",
  });

  const [facilityBody, setFacilitybody] = useState<IValueFacilityBody>({
    Name: "",
    City: "",
    Address: "",
    Owner: "",
    ImageUrl: "",
  });

  const [hasError, setHasError] = useState<boolean>(false);

  useEffect(() => {
    return () => {
      setHasError(false);
    };
  }, []);

  const navigate = useNavigate();

  const createFacilityBody = async (
    values: IValueFacilityForm,
    base64Photo: string
  ) => {
    setFacilitybody((prevState) => ({
      ...prevState,
      Name: values.name,
      City: values.city,
      Address: values.address,
      Owner: values.owner,
      ImageUrl: base64Photo,
    }));

    const updatedFacilityBody = {
      ...facilityBody,
      Name: values.name,
      City: values.city,
      Address: values.address,
      Owner: values.owner,
      ImageUrl: base64Photo,
    };

    await setFacilitybody(updatedFacilityBody);

    return updatedFacilityBody;
  };

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    const base64code: string | undefined = await convertFileToString(event);
    if (base64code === undefined) {
      return setHasError(true);
    } else {
      setHasError(false);
    }

    const test = await createFacilityBody(formState, base64code);

    // Form validation
    if (Object.values(test).some((x) => x === "")) {
      return setHasError(true);
    } else {
      setHasError(false);
    }

    await createFacility(test);

    // redirect
    navigate(`/`);
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0] || null;
    setFormState((prevState) => ({
      ...prevState,
      photo: file,
    }));
  };

  const { theme } = useContext(ThemeContext);

  return (
    <>
      <div className="w-2/5 border-2 border-success px-7 py-5 mx-auto rounded-md md:w-[80%] sm:w-[90%]">
        <form data-testid="form" onSubmit={handleSubmit}>
          <h2 className="m-6 text-center text-2xl text-accent font-bold">
            Add Facility
          </h2>
          <Input
            labelText="Name:"
            type="text"
            placeholder="Facility name"
            name="name"
            onChange={handleInputChange}
            value={formState["name"]}
          ></Input>
          <Input
            labelText="City:"
            type="text"
            placeholder="City"
            name="city"
            onChange={handleInputChange}
            value={formState["city"]}
          ></Input>
          <Input
            labelText="Address:"
            type="text"
            placeholder="Address"
            name="address"
            onChange={handleInputChange}
            value={formState["address"]}
          ></Input>
          <Input
            labelText="Owner:"
            type="text"
            placeholder="Owner"
            name="owner"
            onChange={handleInputChange}
            value={formState["owner"]}
          ></Input>
          <label className="font-bold cursor-pointer" htmlFor="photo">
            <div className="btn bg-secondary hover:text-primary my-5 ">
              <RiLinksLine />
              Upload Photo
            </div>
          </label>
          <input
            className="hidden"
            type="file"
            placeholder="Upload photo"
            name="photo"
            id="photo"
            onChange={handleFileChange}
            accept="image/*"
          ></input>
          <p>{formState.photo ? formState.photo.name : ""}</p>
          {hasError && (
            <span className="text-error">All fields must be filled</span>
          )}
          <button className="btn btn-secondary w-full my-5 " type="submit">
            Submit
          </button>
        </form>
      </div>
    </>
  );
};
