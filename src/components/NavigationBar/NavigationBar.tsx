import { FC, useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { Hamburger } from "../Hamburger/Hamburger";
import { AuthContext } from "../../contexts/AuthContext";

import logo from "../../assets/logos/logo.svg";
import darkLogo from "../../assets/logos/darkLogo.svg";
import { ThemeContext } from "../../contexts";
import {
  IFetchedFacility,
  getOneFacility,
} from "../../services/FacilityServices";

export const NavigationBar: FC = () => {
  const { userData, onLogout, userName } = useContext(AuthContext);
  const { theme } = useContext(ThemeContext);
  const { facilityId } = useParams();
  const [facility, setFacility] = useState<IFetchedFacility>();

  useEffect(() => {
    if (facilityId !== undefined) {
      getOneFacility(Number(facilityId)).then((f) => {
        if (f !== undefined) {
          setFacility(f);
        }
      });
    } else {
      setFacility(undefined);
    }
  }, [facilityId]);

  return (
    <nav className=" fixed h-[88px] w-full z-50 py-3 px-20 bg-secondary flex flex-wrap justify-between sm:px-7 sm:justify-between ">
      <div className="flex items-center">
        <Link to="/">
          <img
            className="w-16 rounded-full"
            src={theme === "lightTheme" ? logo : darkLogo}
            alt="logo"
          />
        </Link>
        <div className="px-5 self-center uppercase text-lg text-primary sm:hidden">
          {facility && facility.name}
        </div>
      </div>

      <div className="self-center flex">
        {userName !== "" && (
          <div className=" px-5 self-center uppercase text-lg text-primary">
            Hi, {userName.split(" ")[0]}!
          </div>
        )}
        {/* TODO FIx this */}
        <ul className="flex flex-wrap justify-end md:hidden">
          <li>
            <Link
              className="btn btn-secondary mx-2 my-1 text-lg md:my-0"
              to="/"
            >
              {/* Change when roles are added */}
              {!userData ? "Home" : "Facilities"}
            </Link>
          </li>

          {!userData && (
            <>
              <li>
                <Link
                  className="btn btn-secondary mx-2 my-1 text-lg sm:my-0"
                  data-testid="login"
                  to="/login"
                >
                  Login
                </Link>
              </li>
              <li>
                <Link
                  className="btn btn-secondary mx-2 my-1 text-lg sm:my-0"
                  data-testid="register"
                  to="/register"
                >
                  Register
                </Link>
              </li>
            </>
          )}
          {userData && (
            <button
              onClick={onLogout}
              data-testid="logout"
              className="btn btn-secondary mx-2 my-1 text-lg sm:my-0"
            >
              Logout
            </button>
          )}
        </ul>
        <div className="flex-1">
          <Hamburger />
        </div>
      </div>
    </nav>
  );
};
function setFacility(f: any) {
  throw new Error("Function not implemented.");
}
