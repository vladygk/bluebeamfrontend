import { ChangeEventHandler, FC, FocusEventHandler } from "react";
import { IErrorMessage, errorMessages } from "../../utils/formValidation";

interface IInput {
  labelText: string;
  type: string;
  placeholder: string;
  name: string;
  onChange?: ChangeEventHandler<HTMLInputElement>;
  value?: string;
  hasError?: boolean;
  onBlur?: FocusEventHandler<HTMLInputElement>;
  disabled?: boolean;
  hidden?: boolean;
}

export const Input: FC<IInput> = (props) => {
  return (
    <div
      className={`my-5 ${props.disabled == true ? "pointer-events-none" : ""} ${
        props.hidden == true ? "hidden" : ""
      }`}
    >
      <label className="font-bold text-accent" htmlFor={props.name}>
        {props.labelText}
      </label>
      <input
        className="border-2 w-full rounded-md h-10 p-3 bg-primary text-accent placeholder-gray-500"
        type={props.type}
        placeholder={props.placeholder}
        name={props.name}
        id={props.name}
        onChange={props.onChange}
        value={props.value}
        onBlur={props.onBlur}
      />
      {props.hasError && (
        <span className="text-error">
          {errorMessages[props.name as keyof IErrorMessage]}
        </span>
      )}
    </div>
  );
};
