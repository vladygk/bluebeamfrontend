import { FC, useContext } from "react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../../contexts/AuthContext";
import { ThemeContext } from "../../contexts";

import logo from "../../assets/logos/logo.svg";
import darkLogo from "../../assets/logos/darkLogo.svg";

export const Hamburger: FC = () => {
  const { userData, onLogout } = useContext(AuthContext);
  const { theme } = useContext(ThemeContext);
  const [isOpen, setIsOpen] = useState<boolean>();

  const handleClick = () => {
    setIsOpen((prev) => !prev);
  };

  const handleLogout = () => {
    onLogout();
    setIsOpen(false);
  };

  useEffect(() => {
    const handleOutsideClick = (event: MouseEvent) => {
      const target = event.target as HTMLElement;
      if (isOpen && !target.closest(".hamburger-menu")) {
        setIsOpen(false);
      }
    };
    window.addEventListener("click", handleOutsideClick);
    return () => {
      window.removeEventListener("click", handleOutsideClick);
    };
  }, [isOpen]);

  return (
    <>
      <div className="hamburger-menu">
        <div className="flex flex-col">
          <button
            onClick={handleClick}
            type="button"
            className={`hidden w-8 h-8  justify-around flex-col flex-wrap z-50 cursor-pointer self-center md:flex`}
          >
            <div
              className={`bg-base-100 block w-8 h-[0.30rem] rounded transition-all origin-[1px] ${
                isOpen ? "rotate-45" : "rotate-0"
              }`}
            />
            <div
              className={`bg-base-100 block w-8 h-[0.30rem] rounded transition-all origin-[1px] ${
                isOpen ? "translate-x-full bg-transparent w-0" : "translate-x-0"
              }`}
            />
            <div
              className={`bg-base-100 block w-8 h-[0.30rem] rounded transition-all origin-[1px] ${
                isOpen ? "rotate-[-45deg]" : "rotate-0"
              }`}
            />
          </button>
        </div>

        {isOpen && (
          <ul className=" flex flex-col absolute z-40 bg-secondary p-2 shadow top-0 left-0 w-full self-center">
            <li>
              <Link to="/" onClick={() => setIsOpen(false)}>
                <img
                  className="w-16 mx-5 my-1 rounded-full"
                  src={theme === "lightTheme" ? logo : darkLogo}
                  alt="logo"
                />
              </Link>
            </li>
            <li className="w-full">
              <Link
                className="btn btn-secondary my-1 w-full text-lg sm:my-0"
                to="/"
                onClick={() => setIsOpen(false)}
              >
                Home
              </Link>
            </li>
            {!userData && (
              <>
                <li w-full>
                  <Link
                    className="btn btn-secondary my-1 w-full text-lg sm:my-0 "
                    to="/login"
                    onClick={() => setIsOpen(false)}
                  >
                    Login
                  </Link>
                </li>
                <li w-full>
                  <Link
                    className="btn btn-secondary my-1 w-full text-lg sm:my-0"
                    to="/register"
                    onClick={() => setIsOpen(false)}
                  >
                    Register
                  </Link>
                </li>
              </>
            )}
            {userData && (
              <button
                onClick={handleLogout}
                data-testid="logout"
                className="btn btn-secondary mx-2 my-1 text-lg sm:my-0"
              >
                Logout
              </button>
            )}
          </ul>
        )}
      </div>
    </>
  );
};
