export const PlaceholderImage = () => {
  // Placeholder image URL
  const placeholderImageUrl = "/src/assets/photos/blurr.avif";

  return (
    <img
      className="border-2 rounded-md border-[gray] w-[100%] h-[350px] lg:h-[250px]"
      src={placeholderImageUrl}
      alt="Placeholder"
    />
  );
};
