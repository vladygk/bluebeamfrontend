import { FC } from "react";

export interface ICommentCard {
  id?: number;
  userName: string;
  text: string;
  createdOn: string;
}

export const CommentCard: FC<ICommentCard> = (props: ICommentCard) => {
  return (
    <div className="flex flex-col border-2 border-success bg-success rounded-md mb-2 mr-3 p-1">
      <div className="flex flex-col p-1">
        <p className="font-bold">{props.userName}</p>
        <p className="text-sm">{props.createdOn}</p>
      </div>
      <div>
        <p className="break-words p-1">{props.text}</p>
      </div>
    </div>
  );
};
