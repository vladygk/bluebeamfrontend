import { useContext, useEffect, useState } from "react";
import { Input } from "../Input";
import { Link, redirect, useNavigate, useParams } from "react-router-dom";
import { ThemeContext } from "../../contexts/ThemeContext";
import { RiLinksLine } from "react-icons/ri";
import convertFileToString from "../../utils/convertFileToString";
import { createSpace } from "../../services/SpaceServices";
import { getOneFacility } from "../../services/FacilityServices";

interface IValueFacilityForm {
  name: string;
  location: string;
  photo: File | null;
}

export interface IValueSpaceBody {
  Name: string;
  Location: string;
  ImageUrl: string;
  FacilityId: number;
}

export const SpaceForm = () => {
  const [formState, setFormState] = useState<IValueFacilityForm>({
    name: "",
    location: "",
    photo: null,
  });

  const [spaceBody, setSpaceBody] = useState<IValueSpaceBody>({
    Name: "",
    Location: "",
    ImageUrl: "",
    FacilityId: 1,
  });
  const [hasError, setHasError] = useState<boolean>(false);

  useEffect(() => {
    return () => {
      setHasError(false);
    };
  }, []);

  const { facilityId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    getOneFacility(Number(facilityId)).catch((e) => navigate("/error"));
  }, []);

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    const base64code: string | undefined = await convertFileToString(event);
    if (base64code === undefined) {
      return setHasError(true);
    } else {
      setHasError(false);
    }
    const spaceData = await createSpaceBody(formState, base64code);

    // Form validation
    if (Object.values(spaceData).some((x) => x === "")) {
      return setHasError(true);
    } else {
      setHasError(false);
    }

    await createSpace(spaceData);

    // redirect
    navigate(`/facility/${facilityId}/spaces`);
  };

  const createSpaceBody = async (
    values: IValueFacilityForm,
    base64Photo: string
  ) => {
    const updatedSpaceBody = {
      ...spaceBody,
      Name: values.name,
      Location: values.location,
      ImageUrl: base64Photo,
      FacilityId: Number(facilityId),
    };
    setSpaceBody(updatedSpaceBody);

    return updatedSpaceBody;
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0] || null;
    setFormState((prevState) => ({
      ...prevState,
      photo: file,
    }));
  };

  return (
    <>
      <div className="w-2/6 border-2 border-success px-7 mx-auto rounded-md m-[20px] md:w-[80%] sm:w-[90%]">
        <form onSubmit={handleSubmit}>
          <h2 className="m-6 text-center text-2xl font-bold text-accent">
            Add Space
          </h2>
          <Input
            labelText="Name:"
            type="text"
            placeholder="Space name"
            name="name"
            onChange={handleInputChange}
            value={formState["name"]}
          ></Input>
          <Input
            labelText="Location:"
            type="text"
            placeholder="Location"
            name="location"
            onChange={handleInputChange}
            value={formState["location"]}
          ></Input>
          <label className="font-bold cursor-pointer" htmlFor="photo">
            <div className="btn bg-accent hover:text-primary my-5">
              <RiLinksLine />
              Upload Photo
            </div>
          </label>
          <input
            className="hidden"
            type="file"
            placeholder="Upload photo"
            name="photo"
            id="photo"
            onChange={handleFileChange}
            accept="image/*"
          ></input>
          <p>{formState.photo ? formState.photo.name : ""}</p>
          {hasError && (
            <span className="text-error">All fields must be filled</span>
          )}
          <button className="btn btn-secondary w-full my-7" type="submit">
            Submit
          </button>
        </form>
      </div>
    </>
  );
};
