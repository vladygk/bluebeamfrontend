export interface IUserData {
  token: string;
}

export const saveToStorage = (key: string, data: string) => {
  if (!data) {
    return;
  }

  localStorage.setItem(key, JSON.stringify(data));
};

export const getUserFromStorage = () => {
  const storageData = localStorage.getItem("user");

  if (!storageData) {
    return null;
  }

  const data = JSON.parse(storageData || "");

  return { token: data["jwtToken"] };
};

export const getNameFromStorage = () => {
  const storageData = localStorage.getItem("userName");

  if (!storageData) {
    return "";
  }

  const data = JSON.parse(storageData);

  return data;
};

export const getUserIdFromStorage = () => {
  const storageData = localStorage.getItem("userId");

  if (!storageData) {
    return "";
  }

  const data = JSON.parse(storageData);

  return data;
};

export const getRoleFromStorage = () => {
  const storageData = localStorage.getItem("userRole");

  if (!storageData) {
    return "";
  }

  const data = JSON.parse(storageData);

  return data;
};

export const getThemeFromStorage = () => {
  const storageData = localStorage.getItem("theme");

  if (!storageData) {
    return null;
  }

  const data = JSON.parse(storageData || "");

  return data;
};

export const removeFromStorage = (key: string) => {
  localStorage.removeItem(key);
};
