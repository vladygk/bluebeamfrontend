const convertFileToString = async (event: any) => {
  const file = event.target.photo.files[0];
  if (file === undefined) {
    return;
  }

  const convertedFile = new Promise<string>((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = (event) => {
      if (event.target !== null) resolve(String(event.target.result));
    };

    reader.onerror = (err) => {
      reject(err);
    };

    reader.readAsDataURL(file);
  });

  const base64Code = await convertedFile; // Await the convertedFile promise
  const splitBase64Code = String(base64Code).split(",")[1];

  return splitBase64Code;
};

export default convertFileToString;
