import { useContext, FC, useState, useEffect } from "react";
import { AuthContext } from "../contexts";
import { useNavigate } from "react-router-dom";

interface IRouteGuard {
  Component: FC<any>;
  DefaultComponent: FC<any>;
}

export const RouteGuard: FC<IRouteGuard> = ({
  Component,
  DefaultComponent,
}) => {
  const { userData } = useContext(AuthContext);

  const [guarded, setGuarded] = useState(false);

  if (userData) {
    return <Component />;
  } else {
    history.replaceState("", "", "/login");

    setGuarded(true);
    return <DefaultComponent />;
  }
};
