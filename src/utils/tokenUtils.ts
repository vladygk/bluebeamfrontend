import { IUserData, getUserFromStorage } from "./localStorage";
import jwt_decode from "jwt-decode";

export interface ITokenPayload {
  aud: string;
  exp: number;
  role?: string;
  emailaddress: string;
  name: string;
  nameidentifier: string;
  iss: string;
}

export const decodeToken: any = async () => {
  const token: IUserData = getUserFromStorage()!;
  if (!token) {
    return null;
  }

  const decoded = jwt_decode(token.token);

  return decoded;
};
