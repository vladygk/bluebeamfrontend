import { ICommentExport } from "../components/Comments/Comments";
import { IPoint } from "../components/Drawing/IPoint";
import { getUserFromStorage } from "../utils/localStorage";

const baseUrl = "https://bluebean.azurewebsites.net/comments";

export const createComment = async (commentData: ICommentExport) => {
  const body = JSON.stringify(commentData);
  console.log(body);

  const response = await fetch(baseUrl, {
    method: "POST",
    headers: {
      "content-type": "application/json",
    },
    body: body,
  });

  return "Comment created";
};

export const getAllForPunch = async (punchId: number) => {
  const response = await fetch(`${baseUrl}?punchId=${punchId}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  const data = await response.json();

  return data;
};
