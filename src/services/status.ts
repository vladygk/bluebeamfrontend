const baseUrl = "https://bluebean.azurewebsites.net/statuses";

export const getAllStatuses = async () => {
  const response = await fetch(baseUrl, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  const data = await response.json();

  return data;
};
