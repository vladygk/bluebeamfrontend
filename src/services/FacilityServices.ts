import { IValueFacilityBody } from "../components/FacilityForm/FacilityForm";
import { getUserFromStorage } from "../utils/localStorage";

export interface IFetchedFacility {
  id: number;
  name: string;
  city: string;
  address: string;
  imageUrl: string;
  owners?: string[];
}

const baseUrl = "https://bluebean.azurewebsites.net/facilities";

export const createFacility = async (facilityData: IValueFacilityBody) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const body = JSON.stringify(facilityData);
  const response = await fetch(`${baseUrl}`, {
    method: "POST",
    body: body,
    headers: {
      "Content-Type": "application/json",
      Authorization: jwt,
    },
  });

  if (!response.ok) throw new Error("Failed to add facility");

  // const data = await response.json();
  // return data;

  return "Created facility";
};

export const getAllFacilities = async () => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(`${baseUrl}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
      Authorization: jwt,
    },
  });

  const data: IFetchedFacility[] = await response.json();
  return data;
};

export const getAllFacilitiesForUser = async (id: number) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(`${baseUrl}?userId=${id}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
      Authorization: jwt,
    },
  });

  const data: IFetchedFacility[] = await response.json();
  return data;
};

export const getOneFacility = async (facilityId: number): Promise<any> => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(`${baseUrl}?facilityId=${facilityId}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
      Authorization: jwt,
    },
  });
  if (!response.ok) throw new Error("Failed to fetch facility");

  const data: IFetchedFacility = await response.json();
  return data;
};
