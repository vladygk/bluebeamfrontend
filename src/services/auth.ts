import { IValueInvite } from "../pages/AddUsers/InviteUsersForm";
import { IValueLogin } from "../pages/Login/Login";
import { IValueRegister } from "../pages/register/Register";
import { getUserFromStorage } from "../utils/localStorage";

export interface IFetchedUser {
  id: string;
  name: string;
  email: string;
  role: string;
}

const baseUrl = "https://bluebean.azurewebsites.net/users";

export const login = async (value: IValueLogin) => {
  const body = JSON.stringify(value);

  const response = await fetch(`${baseUrl}/login`, {
    method: "POST",
    headers: {
      "content-type": "application/json",
    },
    body,
  });

  if (response.status == 203) {
    return;
  }

  if (response.status == 401) {
    throw new Error("Unautorized: Password or email are incorrect");
  }

  if (response.status == 400) {
    throw new Error("Bad request: Data provided isn't in the correct format");
  }

  const userData = await response.json();
  return userData;
};

export const register = async (value: IValueRegister) => {
  const body = JSON.stringify(value);

  const response = await fetch(`${baseUrl}/register`, {
    method: "POST",
    headers: {
      "content-type": "application/json",
    },
    body,
  });

  if (response.status == 203) {
    return;
  }

  if (response.status == 409) {
    throw new Error("Conflict: User with this email already exists");
  }

  if (response.status == 400) {
    throw new Error("Bad request: Data provided isn't in the correct format");
  }

  const userData = await response.json();
  return userData;
};

export const getAllUsers = async () => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(`${baseUrl}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  const data = await response.json();

  return data;
};

export const getOneUser = async (email: string) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(`${baseUrl}?email=${email}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  if (response.status == 204) {
    return "";
  }

  const data = await response.json();

  return data;
};

export const getAllUsersFromFacility = async (id: number) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(`${baseUrl}?facilityId=${id}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  const data = await response.json();

  return data;
};

export const getAllUsersWithRoleFromFacility = async (
  facilityId: number,
  roleId: number
) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(
    `${baseUrl}?facilityId=${facilityId}&roleId=${roleId}`,
    {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    }
  );

  const data = await response.json();

  return data;
};

export const removeUserFromFacility = async (
  userId: number,
  facilityId: number
) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }
  const response = await fetch(
    `${baseUrl}?userId=${userId}&facilityId=${facilityId}`,
    {
      method: "DELETE",
      headers: {
        "content-type": "application/json",
      },
    }
  );

  return "User deleted from facility";
};

export const addUserToFacility = async (userId: number, facilityId: number) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }
  const response = await fetch(
    `${baseUrl}?userId=${userId}&facilityId=${facilityId}`,
    {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
    }
  );

  return "User added in facility";
};

export const sendEmailInvite = async (emailInfo: IValueInvite) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const body = JSON.stringify(emailInfo);

  const response = await fetch(`${baseUrl}/send`, {
    method: "POST",
    headers: {
      "content-type": "application/json",
      Authorization: `Bearer ${jwt}`,
    },
    body,
  });

  if (response.status === 200) {
    return "Email sent successfully";
  } else {
    throw new Error("Failed to send email");
  }
};
