import { IValueAssetBody } from "../components/AssetForm/AssetForm";
import { getUserFromStorage } from "../utils/localStorage";

const baseUrl = `https://bluebean.azurewebsites.net/assets`;

export interface IFetchedAsset {
  id: number;
  name: string;
  addedOn: string;
}

export const createAsset = async (assetData: IValueAssetBody) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const body = JSON.stringify(assetData);

  const response = await fetch(baseUrl, {
    method: "POST",
    headers: {
      "content-type": "application/json",
    },
    body: body,
  });

  // const data = await response.json();

  // return data;

  return "Asset created";
};

export const editAsset = async (editAssetData: IValueAssetBody, id: number) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const body = JSON.stringify(editAssetData);

  const response = await fetch(`${baseUrl}/${id}`, {
    method: "PUT",
    headers: {
      "content-type": "application/json",
    },
    body: body,
  });

  const data = await response.json();

  return data;
};

export const removeAsset = async (assetId: number, facilityId: number) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(
    `${baseUrl}?assetId=${assetId}&facilityId=${facilityId}`,
    {
      method: "DELETE",
      headers: {
        "content-type": "application/json",
      },
    }
  );

  // const data = await response.json();

  // return data;

  return "Asset deleted";
};

export const getAllAssets = async () => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(baseUrl, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  const data = await response.json();

  return data;
};

export const getAllAssetsForFacility = async (facilityId: number) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }
  console.log(baseUrl);

  const response = await fetch(`${baseUrl}?facilityId=${facilityId}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  const data: IFetchedAsset[] = await response.json();

  return data;
};

export const getOneAsset = async (assetId: number) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(`${baseUrl}?assetId=${assetId}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  const data = await response.json();

  return data;
};
