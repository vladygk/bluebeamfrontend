import { Routes, Route } from "react-router-dom";
import { AuthProvider } from "./contexts/";
import { ThemeProvider } from "./contexts/ThemeContext";
import { RouteGuard } from "./utils/RouteGuard";
import { Home } from "./pages/Home/Home";
import { Login } from "./pages/Login/";
import { Register } from "./pages/register/";
import { Dashboard } from "./pages/Dashboard/Dashboard";
import { FacilityPage } from "./pages/Facility";
import { Spaces } from "./pages/Spaces";
import { SpaceContainer } from "./pages/Space/SpaceContainer";
import { Punches } from "./pages/Punches/Punches";
import { Assets } from "./pages/Assets/Assets";
import { LoadingScreen } from "./pages/LoadingScreen/LoadingScreen";
import { Error } from "./pages/404/";
import { Layout } from "./components/Layout/";
import { FacilityForm } from "./components/FacilityForm";
import { AssetForm } from "./components/AssetForm";
import { SpaceForm } from "./components/SpaceForm";

export {
  Routes,
  Route,
  AuthProvider,
  ThemeProvider,
  RouteGuard,
  Home,
  Login,
  Register,
  Dashboard,
  FacilityPage,
  Spaces,
  SpaceContainer,
  Punches,
  Assets,
  LoadingScreen,
  Error,
  Layout,
  FacilityForm,
  AssetForm,
  SpaceForm,
};
