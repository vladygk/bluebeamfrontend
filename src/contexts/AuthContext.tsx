import { FC, FormEvent, createContext, useState } from "react";
import { IValueRegister } from "../pages/register";
import { IValueLogin } from "../pages/Login";
import {
  IUserData,
  getNameFromStorage,
  getRoleFromStorage,
  getUserFromStorage,
  getUserIdFromStorage,
  removeFromStorage,
  saveToStorage,
} from "../utils/localStorage";
import { IHasError } from "../utils/formValidation";
import { StrictButtonGroupProps } from "semantic-ui-react";
import { useNavigate } from "react-router-dom";
import { login, register } from "../services/auth";

export interface IAuthContext {
  userData: IUserData | null;
  onRegister: (
    event: FormEvent<Element>,
    value: IValueRegister,
    error: IHasError,
    repeatPassword: string
  ) => Promise<void>;
  onLogin: (
    event: FormEvent<Element>,
    value: IValueLogin,
    error: IHasError
  ) => Promise<void>;
  onLogout: () => void;
  responseStatusError: string;
  setResponseStatusError: React.Dispatch<React.SetStateAction<string>>;
  userName: string;
  setUserName: React.Dispatch<React.SetStateAction<string>> | undefined;
  userId: string;
  setUserId: React.Dispatch<React.SetStateAction<string>>;
  userRole: string;
  setUserRole: React.Dispatch<React.SetStateAction<string>>;
}

export const AuthContext = createContext<IAuthContext>({
  userData: { token: "" },
  onRegister: (e, value, error, repeatPassword) => {
    return new Promise(() => true);
  },
  onLogin: (e, value, error) => {
    return new Promise(() => true);
  },
  onLogout: () => {
    return;
  },
  responseStatusError: "",
  userName: "",
  setUserName: undefined,
  userId: "",
  setUserId: () => {},
  userRole: "",
  setUserRole: () => {},
  setResponseStatusError: () => {
    return;
  },
});

export const AuthProvider: FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [userData, setUserData] = useState<IUserData | null>(() =>
    getUserFromStorage()
  );

  const [responseStatusError, setResponseStatusError] = useState<string>("");

  const [userName, setUserName] = useState(() => getNameFromStorage());
  const [userId, setUserId] = useState(() => getUserIdFromStorage());
  const [userRole, setUserRole] = useState(() => getRoleFromStorage());

  const navigate = useNavigate();

  const onRegister = async (
    event: FormEvent,
    value: IValueRegister,
    error: IHasError,
    repeatPassword: string
  ) => {
    event.preventDefault();

    if (Object.values(value).some((x) => x === "")) {
      return;
    }

    if (Object.values(error).some((x) => x === true)) {
      return;
    }

    if (repeatPassword != value["password"]) {
      return;
    }

    try {
      const userData = await register(value);
      setResponseStatusError("");
      setUserData(userData);
      saveToStorage("user", userData);
      navigate("/");
    } catch (error: any) {
      const e: Error = error;
      const message = e.message;
      return setResponseStatusError(message);
    }
  };

  const onLogin = async (
    event: FormEvent,
    value: IValueLogin,
    error: IHasError
  ) => {
    event.preventDefault();

    if (Object.values(value).some((x) => x === "")) {
      return;
    }
    if (Object.values(error).some((x) => x === true)) {
      return;
    }

    try {
      const userData = await login(value);
      setResponseStatusError("");
      setUserData(userData);
      saveToStorage("user", userData);
      navigate("/");
    } catch (error: any) {
      const e: Error = error;
      const message = e.message;
      setResponseStatusError(message);
    }
  };

  const onLogout = () => {
    removeFromStorage("user");
    setUserData(null);
    setUserId("");
    setUserName("");
    setUserRole("");
    removeFromStorage("userName");
    removeFromStorage("userId");
    removeFromStorage("userRole");
    navigate("/");
  };

  return (
    <>
      <AuthContext.Provider
        value={{
          userData,
          onRegister,
          onLogin,
          onLogout,
          responseStatusError,
          setResponseStatusError,
          userName,
          setUserName,
          userId,
          setUserId,
          userRole,
          setUserRole,
        }}
      >
        {children}
      </AuthContext.Provider>
    </>
  );
};
