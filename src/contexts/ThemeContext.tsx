import { createContext, useEffect, useState, FC } from "react";
import { getThemeFromStorage, saveToStorage } from "../utils/localStorage";

interface IThemeContext {
  toggleTheme: () => void;
  toggleMantineTable: () => void;
  theme: string;
}

export const ThemeContext = createContext<IThemeContext>({
  toggleTheme: () => {
    console.log();
  },
  toggleMantineTable: () => {
    console.log();
  },
  theme: "",
});

export const ThemeProvider: FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [theme, setTheme] = useState<string>(
    () => getThemeFromStorage() ?? "lightTheme"
  );

  useEffect(() => {
    const htmlTag = document.getElementById("html");
    if (theme) {
      htmlTag?.setAttribute("data-theme", theme);
    }
  }, [theme]);

  const toggleTheme = () => {
    if (theme == "darkTheme") {
      setTheme("lightTheme");

      saveToStorage("theme", "lightTheme");
    } else if (theme == "lightTheme") {
      setTheme("darkTheme");

      saveToStorage("theme", "darkTheme");
    }
  };

  const toggleMantineTable = () => {
    const elements = document.querySelectorAll('[class*="mantine"]');

    if (theme === "darkTheme") {
      for (const el of elements) {
        el.classList.add("dark-table");
      }
    } else if (theme === "lightTheme") {
      for (const el of elements) {
        el.classList.remove("dark-table");
      }
    }
  };

  return (
    <ThemeContext.Provider value={{ toggleTheme, toggleMantineTable, theme }}>
      {children}
    </ThemeContext.Provider>
  );
};
