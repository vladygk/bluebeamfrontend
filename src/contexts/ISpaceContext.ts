import { Dispatch, SetStateAction, createContext } from "react";
import { IPoint } from "../components/Drawing/IPoint";

interface SpaceContext {
  isActiveForm: boolean;
  setIsActiveForm: Dispatch<React.SetStateAction<boolean>>;
  buttonType: string;
  setButtonType: Dispatch<React.SetStateAction<string | undefined>>;
  isDrawable: boolean;
  setIsDrawable: Dispatch<React.SetStateAction<boolean>>;
  points: IPoint[];
  setPoints: Dispatch<React.SetStateAction<IPoint[]>>;
  clickedPointData: IPoint;
  setClickedPointData: Dispatch<SetStateAction<IPoint>>;
  activeButton: string;
  setActiveButton: Dispatch<SetStateAction<string>>;
}
export const SpaceContext = createContext<SpaceContext | undefined>(undefined);
