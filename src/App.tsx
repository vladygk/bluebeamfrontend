import {
  Routes,
  Route,
  AuthProvider,
  ThemeProvider,
  RouteGuard,
  Home,
  Login,
  Register,
  Dashboard,
  FacilityPage,
  Spaces,
  SpaceContainer,
  Punches,
  Assets,
  LoadingScreen,
  Error,
  Layout,
  FacilityForm,
  AssetForm,
  SpaceForm,
} from "./appImports";
import { SpaceProvider } from "./contexts/SpaceProvider";
import { AddUsers } from "./pages/AddUsers/AddUsers";
import { Users } from "./pages/Users";

function App() {
  return (
    <>
      <ThemeProvider>
        <SpaceProvider>
          <AuthProvider>
            <Routes>
              <Route path="/" element={<Layout />}>
                <Route path="/" element={<Home />} />
                <Route path="login" element={<Login />} />
                <Route path="register" element={<Register />} />
                <Route
                  path="facility"
                  element={
                    <RouteGuard
                      Component={FacilityPage}
                      DefaultComponent={Login}
                    />
                  }
                />
                <Route
                  path="facility/:facilityId"
                  element={
                    <RouteGuard
                      Component={Dashboard}
                      DefaultComponent={Login}
                    />
                  }
                />
                <Route
                  path="facility/:facilityId/spaces"
                  element={
                    <RouteGuard Component={Spaces} DefaultComponent={Login} />
                  }
                />
                <Route
                  path="facility/:facilityId/spaces/:spaceId"
                  element={
                    <RouteGuard
                      Component={SpaceContainer}
                      DefaultComponent={Login}
                    />
                  }
                />
                <Route path="/loading" element={<LoadingScreen />} />
                <Route path="/facilities/add" element={<FacilityForm />} />
                <Route
                  path="/facility/:facilityId/space/add"
                  element={<SpaceForm />}
                />
                <Route
                  path="/facility/:facilityId/assets/add"
                  element={<AssetForm />}
                />
                <Route
                  path="/facility/:facilityId/users/add"
                  element={<AddUsers />}
                />
                <Route
                  path="/facility/:facilityId/punches"
                  element={<Punches />}
                />
                <Route
                  path="/facility/:facilityId/assets"
                  element={<Assets />}
                />
                <Route path="/facility/:facilityId/users" element={<Users />} />
              </Route>
              <Route path="/*" element={<Error />} />
              <Route path="/error" element={<Error />} />
            </Routes>
          </AuthProvider>
        </SpaceProvider>
      </ThemeProvider>
    </>
  );
}

export default App;
